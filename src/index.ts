import { ExpressServer } from './server';
import { Trick3DApiApplication } from './application';
import { ApplicationConfig } from '@loopback/core';

export { ExpressServer, Trick3DApiApplication };

export async function main(options: ApplicationConfig = {}) {
  const server = new ExpressServer(options);
  await server.boot();
  await server.start();
  console.log(`Server is running at ${server.lbApp.restServer.config.host}:${server.lbApp.restServer.config.port}`);
}
