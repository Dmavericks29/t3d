// Copyright IBM Corp. 2019. All Rights Reserved.
// Node module: @loopback/example-express-composition
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {ApplicationConfig} from '@loopback/core';
import {Request, Response} from 'express';
import http, {request} from 'http';
import pEvent from 'p-event';
import path from 'path';
import {Trick3DApiApplication} from './application';
import express from 'express';
import {UserTokensRepository} from './repositories';

export class ExpressServer {
  private app: express.Application;
  public readonly lbApp: Trick3DApiApplication;
  private server?: http.Server;

  constructor(options: ApplicationConfig = {}) {
    this.app = express();
    // options.rest.expressSettings = {
    //   'x-powered-by': false,
    //   env: 'production',
    // }

    this.lbApp = new Trick3DApiApplication(options);
    this.app.set('view engine', 'ejs');

    // Expose the front-end assets via Express, not as LB4 route
    this.app.use('/api/v1', this.lbApp.requestHandler);

    // Custom Express routes
    this.app.get('/', function(_req: Request, res: Response) {
      res.sendFile(path.join(__dirname, '../public/express.html'));
    });

    // Password creation url
    this.app.get('/api/verify/:token/:id', function(_req: Request, res: Response) {
      let {token, id} = _req.params;
      http
        .get(
          `${_req.protocol}://${_req.headers.host}/api/v1/user-tokens/${id}`,
          resp => {
            console.log('Got response: ' + resp.statusCode);
            if (resp.statusCode != 200) {
              res.render(path.join(__dirname, '../src/templates/verify'), { token:undefined});
            } else {
              resp.on('data', chunk => {
                res.render(path.join(__dirname, '../src/templates/verify'), {
                  token,
                });
              });
            }
          },
        )
        .on('error', function(e) {
          console.log('Got error: ' + e.message);
        });
    });

     // Password creation url
     this.app.get('/api/verify_new/:token/:id', function(_req: Request, res: Response) {
      let {token, id} = _req.params;
      http
        .get(
          `${_req.protocol}://${_req.headers.host}/api/v1/user-tokens/${id}`,
          resp => {
            console.log('Got response: ' + resp.statusCode);
            if (resp.statusCode != 200) {
              res.render(path.join(__dirname, '../src/templates/verify_new'), { token:undefined});
            } else {
              resp.on('data', chunk => {
                res.render(path.join(__dirname, '../src/templates/verify_new'), {
                  token,
                });
              });
            }
          },
        )
        .on('error', function(e) {
          console.log('Got error: ' + e.message);
        });
    });

    // Serve static files in the public folder
    this.app.use(express.static(path.join(__dirname, '../public')));
  }

  public async boot() {
    await this.lbApp.boot();
  }

  public async start() {
    await this.lbApp.start();
    const port = this.lbApp.restServer.config.port || 3000;
    const host = this.lbApp.restServer.config.host ?? '0.0.0.0';
    this.server = this.app.listen(port, host);
    await pEvent(this.server, 'listening');
  }

  // For testing purposes
  public async stop() {
    if (!this.server) return;
    await this.lbApp.stop();
    this.server.close();
    await pEvent(this.server, 'close');
    this.server = undefined;
  }
}
