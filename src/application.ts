import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication, RestBindings} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import * as path from 'path';
import {MySequence} from './sequence';
import {
  AuthenticationComponent,
  registerAuthenticationStrategy,
} from '@loopback/authentication';
import {JWTAuthenticationStrategy} from './authentication-strategies/jwt-strategy';
import {BindingKey} from '@loopback/context';
import {SECURITY_SCHEME_SPEC} from './utils/security-spec';
import {
  TokenServiceBindings,
  TokenServiceConstants,
  PasswordHasherBindings,
  UserServiceBindings,
  SecurityBindings,
  MailServiceBindings,
  UserControllerBindings,
  ReposirotyBindings,
  ImageUploadServiceBindings,
  CustomerErrorBindings,
  AssetControllerBindings,
} from './keys';
import {JWTService} from './services/jwt-service';
import {BcryptHasher} from './services/hash.password.bcryptjs';
import {MyUserService} from './services/user-service';

import {
  AuthorizationComponent,
  AuthorizationTags,
} from '@loopback/authorization';
import {createEnforcer} from './services/enforcer';
import {CasbinAuthorizationProvider} from './services/authorizor';
import {MailerService} from './services';
import {UserController, AssetController} from './controllers';
import {UserTokensRepository} from './repositories';
import {S3ImageUploadService} from './services/s-3-image-upload.service';
import { CustomRejectProvider } from './providers/custom-reject.provider';

/**
 * Information from package.json
 */
export interface PackageInfo {
  name: string;
  version: string;
  description: string;
}
export const PackageKey = BindingKey.create<PackageInfo>('application.package');

const pkg: PackageInfo = require('../package.json');

export class Trick3DApiApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    /*
       This is a workaround until an extension point is introduced
       allowing extensions to contribute to the OpenAPI specification
       dynamically.
    */
    this.api({
      openapi: '3.0.0',
      info: {title: pkg.name, version: pkg.version},
      paths: {},
      components: {securitySchemes: SECURITY_SCHEME_SPEC},
      servers: [{url: '/'}],
    });

    this.setUpBindings();

    this.component(AuthenticationComponent);
    this.component(AuthorizationComponent);

    this.bind('casbin.enforcer').toDynamicValue(createEnforcer);
    this.bind('authorizationProviders.casbin-provider')
      .toProvider(CasbinAuthorizationProvider)
      .tag(AuthorizationTags.AUTHORIZER);

    registerAuthenticationStrategy(this, JWTAuthenticationStrategy);
    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
   /*  this.bind(RestExplorerBindings.CONFIG).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent); */

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  setUpBindings(): void {
    // Bind package.json to the application context
    // this.bind(RestBindings.SequenceActions.REJECT).toProvider(CustomRejectProvider);
    this.bind(PackageKey).to(pkg);

    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      TokenServiceConstants.TOKEN_SECRET_VALUE,
    );

    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
      TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE,
    );

    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);
    this.bind(TokenServiceBindings.JWT_SERVICE).toClass(JWTService);

    // // Bind bcrypt hash services
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);

    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService);
    this.bind(SecurityBindings.USER).to('');

    this.bind(UserControllerBindings.USER_CTRL).toClass(UserController);
    this.bind(AssetControllerBindings.ASSET_CTRL).toClass(AssetController);
    this.bind(ReposirotyBindings.USER_TOKEN_REP).toClass(UserTokensRepository);

    this.bind(MailServiceBindings.MAIL).toClass(MailerService);

    this.bind(ImageUploadServiceBindings.S3_IMG_UPLOAD).toClass(
      S3ImageUploadService,
    );
    
    this.bind(CustomerErrorBindings.CUSTOM_ERRORS).to([]);
    this.bind(CustomerErrorBindings.CUSTOM_ERROR_CODE).to(0);
  }
}
