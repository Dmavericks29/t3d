import {DefaultCrudRepository} from '@loopback/repository';
import {Role, RoleRelations} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RoleRepository extends DefaultCrudRepository<
  Role,
  typeof Role.prototype.RoleID,
  RoleRelations
> {
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
  ) {
    super(Role, dataSource);
  }
}
