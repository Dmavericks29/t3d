import {DefaultCrudRepository} from '@loopback/repository';
import {UserType, UserTypeRelations} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserTypeRepository extends DefaultCrudRepository<
  UserType,
  typeof UserType.prototype.TypeID,
  UserTypeRelations
> {
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
  ) {
    super(UserType, dataSource);
  }
}
