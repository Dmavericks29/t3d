import {DefaultCrudRepository, BelongsToAccessor, repository} from '@loopback/repository';
import {AssetUploads, AssetUploadsRelations, AssetPicture} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import { AssetPictureRepository } from './asset-picture.repository';

export class AssetUploadsRepository extends DefaultCrudRepository<
  AssetUploads,
  typeof AssetUploads.prototype.AssetUploadID,
  AssetUploadsRelations
> {

  public readonly assetPicture: BelongsToAccessor<
    AssetPicture,
    typeof AssetUploads.prototype.AssetUploadID
  >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('AssetPictureRepository')
    assetPictureRepositoryGetter: Getter<AssetPictureRepository>,
  ) {
    super(AssetUploads, dataSource);

    this.assetPicture = this.createBelongsToAccessorFor(
      'assetPicture',
      assetPictureRepositoryGetter,
    );
  }
}
