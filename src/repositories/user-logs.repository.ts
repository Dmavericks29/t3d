import { DefaultCrudRepository } from '@loopback/repository';
import { UserLogs } from '../models';
import { TrickmSqlDbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class UserLogsRepository extends DefaultCrudRepository<
  UserLogs,
  typeof UserLogs.prototype.LogID
  > {
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
  ) {
    super(UserLogs, dataSource);
  }
}
