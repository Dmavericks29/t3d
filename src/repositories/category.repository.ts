import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Category, CategoryRelations, Client } from '../models';
import { TrickmSqlDbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { ClientRepository } from './client.repository';

export class CategoryRepository extends DefaultCrudRepository<
  Category,
  typeof Category.prototype.CategoryID,
  CategoryRelations
  > {

  public readonly client: BelongsToAccessor<
    Client,
    typeof Category.prototype.CategoryID
  >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('ClientRepository')
    clientRepositoryGetter: Getter<ClientRepository>,
  ) {
    super(Category, dataSource);
    this.client = this.createBelongsToAccessorFor(
      'client',
      clientRepositoryGetter,
    );
    this.registerInclusionResolver('client', this.client.inclusionResolver);
  }
}
