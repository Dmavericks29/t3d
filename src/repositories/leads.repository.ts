import {DefaultCrudRepository} from '@loopback/repository';
import {Leads, LeadsRelations} from '../models/leads.model';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LeadsRepository extends DefaultCrudRepository<
  Leads,
  typeof Leads.prototype.LeadID,
  LeadsRelations
> {
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
  ) {
    super(Leads, dataSource);
  }
}
