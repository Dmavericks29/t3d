import {DefaultCrudRepository} from '@loopback/repository';
import {UserTokens, UserTokensRelations} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserTokensRepository extends DefaultCrudRepository<
  UserTokens,
  typeof UserTokens.prototype.id,
  UserTokensRelations
> {
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
  ) {
    super(UserTokens, dataSource);
  }
}
