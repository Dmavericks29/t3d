import {DefaultCrudRepository, BelongsToAccessor, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {AssetPicture, AssetPictureRelations, Asset, AssetUploads} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import { AssetRepository } from './asset.repository';
import { AssetUploadsRepository } from './asset-uploads.repository';

export class AssetPictureRepository extends DefaultCrudRepository<
  AssetPicture,
  typeof AssetPicture.prototype.PictureID,
  AssetPictureRelations
> {

  public readonly asset: BelongsToAccessor<
    Asset,
    typeof AssetPicture.prototype.PictureID
  >;

  public readonly assetUploads: HasManyRepositoryFactory<
    AssetUploads,
    typeof AssetPicture.prototype.PictureID
  >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('AssetRepository')
    assetRepositoryGetter: Getter<AssetRepository>,
    @repository.getter('AssetUploadsRepository')
    protected assetUploadsRepositoryGetter: Getter<AssetUploadsRepository>,
  ) {
    super(AssetPicture, dataSource);

    this.asset = this.createBelongsToAccessorFor(
      'asset',
      assetRepositoryGetter,
    );

    this.assetUploads = this.createHasManyRepositoryFactoryFor(
      'assetUploads',
      assetUploadsRepositoryGetter
    );

    this.registerInclusionResolver('assetUploads', this.assetUploads.inclusionResolver);
  }
}
