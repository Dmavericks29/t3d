import {DefaultCrudRepository} from '@loopback/repository';
import {UserSessions, UserSessionsRelations} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserSessionsRepository extends DefaultCrudRepository<
  UserSessions,
  typeof UserSessions.prototype.SessionID,
  UserSessionsRelations
> {
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
  ) {
    super(UserSessions, dataSource);
  }
}
