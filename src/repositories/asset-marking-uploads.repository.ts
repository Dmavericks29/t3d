import {DefaultCrudRepository, BelongsToAccessor, repository} from '@loopback/repository';
import {
  AssetMarkingUploads,
  AssetMarkingUploadsRelations,
  AssetMarking,
} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import { AssetMarkingRepository } from './asset-marking.repository';

export class AssetMarkingUploadsRepository extends DefaultCrudRepository<
  AssetMarkingUploads,
  typeof AssetMarkingUploads.prototype.AssetMarkingUploadID,
  AssetMarkingUploadsRelations
> {
  public readonly assetMarking: BelongsToAccessor<
    AssetMarking,
    typeof AssetMarkingUploads.prototype.AssetMarkingUploadID
  >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('AssetMarkingRepository')
    assetMarkingRepositoryGetter: Getter<AssetMarkingRepository>,
  ) {
    super(AssetMarkingUploads, dataSource);

    this.assetMarking = this.createBelongsToAccessorFor(
      'assetMarking',
      assetMarkingRepositoryGetter,
    );
  }
}
