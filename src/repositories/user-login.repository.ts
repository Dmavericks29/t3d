import { DefaultCrudRepository } from '@loopback/repository';
import { UserLogin, UserLoginRelations } from '../models';
import { TrickmSqlDbDataSource } from '../datasources';
import { inject } from '@loopback/core';
// import { UserRepository } from './user.repository';

export class UserLoginRepository extends DefaultCrudRepository<
  UserLogin,
  typeof UserLogin.prototype.LoginID,
  UserLoginRelations
  > {

  // public readonly user: BelongsToAccessor<
  //   User,
  //   typeof UserLogin.prototype.LoginID
  // >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    // @repository.getter('UserRepository')
    // protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(UserLogin, dataSource);
    // this.user = this.createBelongsToAccessorFor(
    //   'user',
    //   userRepositoryGetter,
    // );
  }
}
