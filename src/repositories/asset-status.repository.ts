import {DefaultCrudRepository} from '@loopback/repository';
import {AssetStatus, AssetStatusRelations} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AssetStatusRepository extends DefaultCrudRepository<
  AssetStatus,
  typeof AssetStatus.prototype.StatusID,
  AssetStatusRelations
> {
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
  ) {
    super(AssetStatus, dataSource);
  }
}
