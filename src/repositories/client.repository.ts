import { DefaultCrudRepository, repository, HasManyRepositoryFactory } from '@loopback/repository';
import { Client, User, Asset } from '../models';
import { TrickmSqlDbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { UserRepository } from './user.repository';
import { AssetRepository } from './asset.repository';

export class ClientRepository extends DefaultCrudRepository<
  Client,
  typeof Client.prototype.ClientID
  > {

    public users: HasManyRepositoryFactory<User, typeof Client.prototype.ClientID>;
    public assets: HasManyRepositoryFactory<Asset, typeof Client.prototype.ClientID>;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository(UserRepository) protected userRepository: UserRepository,
    // @repository(AssetRepository) protected assetRepository: AssetRepository,
    @repository.getter('AssetRepository')
    protected assetRepositoryGetter: Getter<AssetRepository>,
  ) {
    super(Client, dataSource);
    this.users = this.createHasManyRepositoryFactoryFor(
      'users',
      async () => userRepository
    );

    this.assets = this.createHasManyRepositoryFactoryFor(
      'assets',
      assetRepositoryGetter
    )
    
    this.registerInclusionResolver('users', this.users.inclusionResolver);
    this.registerInclusionResolver('assets', this.assets.inclusionResolver);
  }
}
