import {DefaultCrudRepository, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {AssetMarking, AssetMarkingRelations} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import { AssetMarkingUploads } from '../models/asset-marking-uploads.model';
import { AssetMarkingUploadsRepository } from './asset-marking-uploads.repository';

export class AssetMarkingRepository extends DefaultCrudRepository<
  AssetMarking,
  typeof AssetMarking.prototype.MarkingID,
  AssetMarkingRelations
> {

  public readonly assetMarkingUploads: HasManyRepositoryFactory<
    AssetMarkingUploads,
    typeof AssetMarking.prototype.MarkingID
  >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('AssetMarkingUploadsRepository')
    protected assetMarkingUploadsRepositoryGetter: Getter<AssetMarkingUploadsRepository>,
  ) {
    super(AssetMarking, dataSource);

    this.assetMarkingUploads = this.createHasManyRepositoryFactoryFor(
      'assetMarkingUploads',
      assetMarkingUploadsRepositoryGetter
    );

    this.registerInclusionResolver('assetMarkingUploads', this.assetMarkingUploads.inclusionResolver);
  }
}
