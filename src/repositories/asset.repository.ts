import {DefaultCrudRepository, HasManyRepositoryFactory, repository, BelongsToAccessor, HasOneRepositoryFactory} from '@loopback/repository';
import {Asset, AssetRelations, AssetMarking, Client, Category, AssetPicture} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import { AssetMarkingRepository } from './asset-marking.repository';
import { ClientRepository } from './client.repository';
import { CategoryRepository } from './category.repository';
import { AssetPictureRepository } from './asset-picture.repository';

export class AssetRepository extends DefaultCrudRepository<
  Asset,
  typeof Asset.prototype.AssetID,
  AssetRelations
> {
  public readonly assetMarking: HasManyRepositoryFactory<
    AssetMarking,
    typeof Asset.prototype.AssetID
  >;

  public readonly assetPicture: HasOneRepositoryFactory<
    AssetPicture,
    typeof Asset.prototype.AssetID
  >;

  public readonly client: BelongsToAccessor<
    Client,
    typeof Asset.prototype.AssetID
  >;

  public readonly category: BelongsToAccessor<
    Category,
    typeof Asset.prototype.AssetID
  >;
  
  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('ClientRepository')
    clientRepositoryGetter: Getter<ClientRepository>,
    @repository.getter('CategoryRepository')
    categoryRepositoryGetter: Getter<CategoryRepository>,
    @repository.getter('AssetMarkingRepository')
    protected assetMarkingRepositoryGetter: Getter<AssetMarkingRepository>,
    @repository.getter('AssetPictureRepository')
    protected assetPictureRepositoryGetter: Getter<AssetPictureRepository>,
  ) {
    super(Asset, dataSource);

    this.assetMarking = this.createHasManyRepositoryFactoryFor(
      'assetMarking',
      assetMarkingRepositoryGetter
    );

    this.assetPicture = this.createHasOneRepositoryFactoryFor(
      'assetPicture',
      assetPictureRepositoryGetter
    );

    this.client = this.createBelongsToAccessorFor(
      'client',
      clientRepositoryGetter,
    );

    this.category = this.createBelongsToAccessorFor(
      'category',
      categoryRepositoryGetter,
    );

    this.registerInclusionResolver('client', this.client.inclusionResolver);
    this.registerInclusionResolver('assetPicture', this.assetPicture.inclusionResolver);
    this.registerInclusionResolver('category', this.category.inclusionResolver);
    this.registerInclusionResolver('assetMarking', this.assetMarking.inclusionResolver);
  }
}
