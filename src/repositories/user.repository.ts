import { DefaultCrudRepository, HasManyRepositoryFactory, HasOneRepositoryFactory, repository, BelongsToAccessor } from '@loopback/repository';
import { User, UserRole, Role, UserTokens, Client, UserSessions } from '../models';
import { TrickmSqlDbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { UserLogs } from '../models/user-logs.model';
import { UserLogin } from '../models/user-login.model';
import { UserLogsRepository } from './user-logs.repository';
import { UserLoginRepository } from './user-login.repository';
import { UserRoleRepository } from './user-role.repository';
import { RoleRepository } from './role.repository';
import { UserSessionsRepository } from './user-sessions.repository';

export type Credentials = {
  Email: string;
  Password: string;
};

export type AsClientCredentials = {
  Email: string;
};

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.UserID
  > {

  public userLogs: HasManyRepositoryFactory<UserLogs, typeof User.prototype.UserID>;

  public readonly userLogin: HasOneRepositoryFactory<
    UserLogin,
    typeof User.prototype.UserID
  >;

  public readonly userSession: HasOneRepositoryFactory<
    UserSessions,
    typeof User.prototype.UserID
  >;

  public readonly userRole: HasOneRepositoryFactory<
    UserRole,
    typeof User.prototype.UserID
  >;

  public readonly userToken: HasOneRepositoryFactory<
    UserTokens,
    typeof User.prototype.UserID
  >;

  public readonly role: BelongsToAccessor<
    Role,
    typeof User.prototype.UserID
  >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('RoleRepository')
    roleRepositoryGetter: Getter<RoleRepository>,
    @repository(UserLogsRepository) protected userLogsRepository: UserLogsRepository,
    @repository.getter('UserLoginRepository')
    protected userLoginRepositoryGetter: Getter<UserLoginRepository>,
    @repository.getter('UserSessionsRepository')
    protected userSessionsRepositoryGetter: Getter<UserSessionsRepository>,
    @repository.getter('UserRoleRepository')
    protected userRoleRepositoryGetter: Getter<UserRoleRepository>
  ) {
    super(User, dataSource);
    this.role = this.createBelongsToAccessorFor(
      'role',
      roleRepositoryGetter,
    );

    this.userLogin = this.createHasOneRepositoryFactoryFor(
      'userLogin',
      userLoginRepositoryGetter
    );

    this.userSession = this.createHasOneRepositoryFactoryFor(
      'userSession',
      userSessionsRepositoryGetter
    );

    this.userRole = this.createHasOneRepositoryFactoryFor(
      'userRole',
      userRoleRepositoryGetter
    );
    this.userRole = this.createHasOneRepositoryFactoryFor(
      'userToken',
      userRoleRepositoryGetter
    );
    this.userLogs = this.createHasManyRepositoryFactoryFor(
      'userLogs',
      async () => userLogsRepository
    )
  }

  async findCredentials(
    UserID: typeof User.prototype.UserID,
  ): Promise<UserLogin | undefined> {
    try {
      return await this.userLogin(UserID).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }

  async findRole(
    UserID: typeof User.prototype.UserID,
  ): Promise<UserRole | undefined> {
    try {
      return await this.userRole(UserID).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }

}
