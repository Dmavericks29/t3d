import {DefaultCrudRepository,BelongsToAccessor, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {AssetBundle, AssetBundleRelations,Asset} from '../models';
import {TrickmSqlDbDataSource} from '../datasources';
import {inject,Getter} from '@loopback/core';
import { AssetRepository } from './asset.repository';

export class AssetBundleRepository extends DefaultCrudRepository<
  AssetBundle,
  typeof AssetBundle.prototype.BundleID,
  AssetBundleRelations
> {

    public readonly asset: BelongsToAccessor<
      Asset,
      typeof AssetBundle.prototype.BundleID
    >;

  constructor(
    @inject('datasources.trickmSqlDB') dataSource: TrickmSqlDbDataSource,
    @repository.getter('AssetRepository')
    assetRepositoryGetter: Getter<AssetRepository>,
  ) {
    super(AssetBundle, dataSource);
    this.asset = this.createBelongsToAccessorFor(
      'asset',
      assetRepositoryGetter,
      
    );
    
  }
}
