// Copyright IBM Corp. 2019. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { BindingKey } from '@loopback/context';
import { PasswordHasher } from './services/hash.password.bcryptjs';
import { TokenService, UserService } from '@loopback/authentication';
import { User } from './models';
import { Credentials, UserTokensRepository } from './repositories';
import { MailerService } from './services';
import { UserController, AssetController } from './controllers';
import { S3ImageUploadService } from './services/s-3-image-upload.service';
import { JWTService } from './services/jwt-service';

export namespace TokenServiceConstants {
  export const TOKEN_SECRET_VALUE = 'gqTuvqHjGpQYNivZ0ysgdBAiWZwrWsR5';
  export const TOKEN_EXPIRES_IN_VALUE = '3600';
}

export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret',
  );
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
    'authentication.jwt.expires.in.seconds',
  );
  export const TOKEN_SERVICE = BindingKey.create<TokenService>(
    'services.authentication.jwt.tokenservice',
  );
  export const JWT_SERVICE = BindingKey.create<JWTService>(
    'services.jwt.tokenservice',
  );
  
}

export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER = BindingKey.create<PasswordHasher>(
    'services.hasher',
  );
  export const ROUNDS = BindingKey.create<number>('services.hasher.round');
}

export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<UserService<User, Credentials>>(
    'services.user.service',
  );
}

export namespace UserControllerBindings {
  export const USER_CTRL = BindingKey.create<UserController>(
    'controller.user',
  );
}

export namespace AssetControllerBindings {
  export const ASSET_CTRL = BindingKey.create<AssetController>(
    'controller.asset',
  );
}

export namespace ReposirotyBindings {
  export const USER_TOKEN_REP = BindingKey.create<UserTokensRepository>(
    'repository.userToken'
  )
}

export namespace SecurityBindings {
  export const USER = BindingKey.create(
    'security.user'
  )
}

export namespace MailServiceBindings {
  export const MAIL = BindingKey.create<MailerService>(
    'services.mail'
  )
}

export namespace ImageUploadServiceBindings {
  export const S3_IMG_UPLOAD = BindingKey.create<S3ImageUploadService>(
    'services.s3image.upload'
  )
}

export namespace CustomerErrorBindings {
  export const CUSTOM_ERRORS = BindingKey.create<Array<Object>>('customErrors');
  export const CUSTOM_ERROR_CODE = BindingKey.create<number>('customErrorCode');
}