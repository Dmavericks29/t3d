import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  RestBindings,
  Response,
  Request,
  HttpErrors,
} from '@loopback/rest';
import {AssetMarking, AssetPicture} from '../models';
import {
  AssetMarkingRepository,
  AssetPictureRepository,
  AssetRepository,
  AssetMarkingUploadsRepository,
  AssetUploadsRepository,
} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import multer from 'multer';
import {ImageUploadServiceBindings, AssetControllerBindings} from '../keys';
import {S3ImageUploadService} from '../services';
import _ from 'lodash';
import { AssetController } from './asset.controller';

@authenticate('jwt')
export class AssetMarkingController {
  constructor(
    @repository(AssetMarkingRepository)
    public assetMarkingRepository: AssetMarkingRepository,
    @repository(AssetMarkingUploadsRepository)
    public assetMarkingUploadsRepository: AssetMarkingUploadsRepository,
    @repository(AssetPictureRepository)
    public assetPictureRepository: AssetPictureRepository,
    @repository(AssetUploadsRepository)
    public assetUploadsRepository: AssetUploadsRepository,
    @repository(AssetRepository)
    public assetRepository: AssetRepository,
    @inject(ImageUploadServiceBindings.S3_IMG_UPLOAD)
    private s3ImageUploadService: S3ImageUploadService,
    @inject(AssetControllerBindings.ASSET_CTRL)
    private _AssetController: AssetController,
  ) {}

  @post('/asset-markings', {
    responses: {
      '200': {
        description: 'AssetMarking model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(AssetMarking)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'multipart/form-data': {
          'x-parser': 'stream',
          schema: {
            type: 'object',
          },
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
    @param.query.string('cid') ClientID: string,
  ): Promise<any> {
    const upload = multer({dest: './public/uploads/'});
    return new Promise<object>((resolve, reject) => {
      upload.any()(request, response, async (err: any) => {
        if (err) {
          reject(err);
        } else {
          try {
            let {
                Title,
                Description,
                MarkingNumber,
                PictureID,
                AssetID,
                ProductVideoURL,
                Name,
                CategoryID,
              } = request.body,
              signedImageURL,
              signedVideoURL,
              MarkingImgURL: any,
              ProductVideo: any,
              ProductImage: any,
              MarkingVideoURL: any,
              files: any = request.files;

            if (Name || CategoryID) {
              await this.assetRepository.updateById(AssetID, {
                Name,
                CategoryID,
              });
            }

            let fileTypeArr: Array<any> = [];

            let ClientName = (await this.assetRepository.client(AssetID))
              .FullName;
            ClientName = ClientName.replace(/[^\w\s]/gi, '')
              .split(' ')
              .join('_');
            let AssetName = Name.replace(/[^\w\s]/gi, '')
              .split(' ')
              .join('_');

            let aMarkings = await this.assetMarkingRepository.find({
              where: {AssetID},
            });
            let picData = await this.assetPictureRepository.findOne({
              where: {AssetID},
            });
            
            if (!picData) {
              
              await this.assetPictureRepository.create({
                AssetID:AssetID,
                
                ProductVideoURL: ProductVideoURL //zipURL.Key,
              });
            } else {
              
              picData.ProductVideoURL = ProductVideoURL
              picData = await this.assetPictureRepository.save(picData);
            }
            

            let isFound = _.find(
              aMarkings,
              x => x.MarkingNumber == MarkingNumber,
            );
            let markData:any;
            if (!isFound) {
              markData = await this.assetMarkingRepository.create({
                Title,
                Description,
                MarkingNumber,
                AssetID
              });
            } else {
              isFound.Title = Title;
              isFound.Description = Description;
              markData = await this.assetMarkingRepository.save(isFound);
            }
            let pImagePromise=[];
            let pVideoPromise=[];
            let mImagePromise=[];
            let mVideoPromise=[];
            for (let index = 0; index < files.length; index++) {
              const element = files[index];
              if (element.fieldname == 'ProductImage') {
                pImagePromise.push(await this.s3ImageUploadService.uploadFileToS3(
                        element,
                        {path: `${ClientName}/${AssetName}`, type:element.mimetype, name:`ProductImage_${+new Date}`},
                      ));
              } else if (element.fieldname == 'ProductVideo') {
                pVideoPromise.push(await this.s3ImageUploadService.uploadFileToS3(
                        element,
                        {path: `${ClientName}/${AssetName}`, type:element.mimetype, name:`ProductVideo_${+new Date}`},
                      ));
              } else if (element.fieldname == 'MarkingImgURL') {
                mImagePromise.push(await this.s3ImageUploadService.uploadFileToS3(
                        element,
                        {path: `${ClientName}/${AssetName}`, type:element.mimetype, name:`MarkingImgURL-${MarkingNumber}_${+new Date}`},
                      ));
              } else {
                mVideoPromise.push(await this.s3ImageUploadService.uploadFileToS3(
                        element,
                        {path: `${ClientName}/${AssetName}`, type:element.mimetype, name:`MarkingVideoURL-${MarkingNumber}_${+new Date}`},
                      ));
              }
            }

            MarkingImgURL = await Promise.all(mImagePromise);
            MarkingVideoURL = await Promise.all(mVideoPromise);
            ProductImage = await Promise.all(pImagePromise);
            ProductVideo = await Promise.all(pVideoPromise);
            console.log('in post')
            console.log(ProductImage);
            console.log(ProductVideo);
            console.log(MarkingVideoURL);
            console.log(MarkingImgURL);
            if (MarkingImgURL.length>0) {
              MarkingImgURL = _.map(MarkingImgURL, item => { return { MarkingID: markData.MarkingID, URL: item.Key, Type:'image'} });
            }
            if (MarkingVideoURL.length>0) {
              MarkingVideoURL = _.map(MarkingVideoURL, item => { return { MarkingID: markData.MarkingID, URL: item.Key, Type:'video'} });
            }
            let aMUploadData = [...MarkingImgURL, ...MarkingVideoURL];
            let markingUploads = await this.assetMarkingUploadsRepository.createAll(aMUploadData);
            aMUploadData = _.map(markingUploads, async item=>{
              return {URL:await this.s3ImageUploadService.getSignedURL(item.URL), Type:item.Type};
            });

            if (ProductImage.length>0) {
              ProductImage = _.map(ProductImage, item => { return { PictureID, URL: item.Key, Type:'image'} });
            }
            if (ProductVideo.length>0) {
              ProductVideo = _.map(ProductVideo, item => { return { PictureID, URL: item.Key, Type:'video'} });
            }
            let uploadData = [...ProductImage, ...ProductVideo];
            let assetUploads = await this.assetUploadsRepository.createAll(uploadData);
            uploadData = _.map(assetUploads, async item=>{
              return {URL:await this.s3ImageUploadService.getSignedURL(item.URL), Type:item.Type};
            });

            // let asset = this._AssetController.findById(AssetID)
            resolve({assetUploads,markingUploads});

          } catch (error) {
            reject(error);
          }
        }
      });
    });
  }

  @get('/asset-markings/count', {
    responses: {
      '200': {
        description: 'AssetMarking model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(AssetMarking))
    where?: Where<AssetMarking>,
  ): Promise<Count> {
    return this.assetMarkingRepository.count(where);
  }

  @get('/asset-markings', {
    responses: {
      '200': {
        description: 'Array of AssetMarking model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AssetMarking, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(AssetMarking))
    filter?: Filter<AssetMarking>,
  ): Promise<AssetMarking[]> {
    return this.assetMarkingRepository.find(filter);
  }

  @patch('/asset-markings', {
    responses: {
      '200': {
        description: 'AssetMarking PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AssetMarking, {partial: true}),
        },
      },
    })
    assetMarking: AssetMarking,
    @param.query.object('where', getWhereSchemaFor(AssetMarking))
    where?: Where<AssetMarking>,
  ): Promise<Count> {
    return this.assetMarkingRepository.updateAll(assetMarking, where);
  }

  @get('/asset-markings/{id}', {
    responses: {
      '200': {
        description: 'AssetMarking model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AssetMarking, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(AssetMarking))
    filter?: Filter<AssetMarking>,
  ): Promise<AssetMarking> {
    return this.assetMarkingRepository.findById(id, filter);
  }

  @patch('/asset-markings/{id}', {
    responses: {
      '204': {
        description: 'AssetMarking PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AssetMarking, {partial: true}),
        },
      },
    })
    assetMarking: AssetMarking,
  ): Promise<void> {
    await this.assetMarkingRepository.updateById(id, assetMarking);
  }

  @put('/asset-markings/{id}', {
    responses: {
      '204': {
        description: 'AssetMarking PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() assetMarking: AssetMarking,
  ): Promise<void> {
    await this.assetMarkingRepository.replaceById(id, assetMarking);
  }

  @del('/asset-markings/{id}', {
    responses: {
      '204': {
        description: 'AssetMarking DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    // await this.assetMarkingRepository.deleteById(id);
    try{
      let mUploads = await this.assetMarkingUploadsRepository.findById(id);
      if(!mUploads) throw new HttpErrors.BadRequest(`Marking upload not found`);
      await this.assetMarkingUploadsRepository.deleteById(id);
      await this.s3ImageUploadService.deleteFileS3(mUploads.URL);
    }catch(error){
      console.log(error)
      throw new HttpErrors.BadRequest(`Something went wrong. Please try again.`);
    }
  }
}
