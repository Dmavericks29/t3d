import { UserProfile } from "@loopback/security";

export interface MyUserProfile extends UserProfile {
  id: string;
  email: string;
  name: string;
  clientID:string;
  role: number;
}

export const UserProfileSchema = {
  type: 'object',
  required: ['UserID'],
  properties: {
    UserID: { type: 'string' },
    Email: { type: 'string' },
    name: { type: 'string' },
    ClientID:{type:'string'},
    RoleID: { type: 'number' }
  },
};

const CredentialsSchema = {
  type: 'object',
  required: ['Email', 'Password'],
  properties: {
    Email: {
      type: 'string',
      format: 'email',
    },
    Password: {
      type: 'string',
      /* minLength: 8, */
    },
  },
};

const LoginAsClientSchema = {
  type: 'object',
  required: ['Email'],
  properties: {
    Email: {
      type: 'string',
      format: 'email',
    }
  },
};

export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': { schema: CredentialsSchema },
  },
};

export const LoginAsClientRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': { schema: LoginAsClientSchema },
  },
};
