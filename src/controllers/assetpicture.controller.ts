import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  Request,
  RestBindings,
  Response,
  HttpErrors,
} from '@loopback/rest';
import {AssetPicture} from '../models';
import {AssetPictureRepository, AssetUploadsRepository} from '../repositories';
import {inject} from '@loopback/core';
import multer from 'multer';
import { authenticate } from '@loopback/authentication';
import { S3ImageUploadService } from '../services';
import { ImageUploadServiceBindings } from '../keys';

@authenticate('jwt')
export class AssetpictureController {
  constructor(
    @repository(AssetPictureRepository)
    public assetPictureRepository: AssetPictureRepository,
    @repository(AssetUploadsRepository)
    public assetUploadsRepository: AssetUploadsRepository,
    @inject(ImageUploadServiceBindings.S3_IMG_UPLOAD)
    private s3ImageUploadService: S3ImageUploadService,
  ) {}

  @post('/asset-pictures', {
    responses: {
      '200': {
        description: 'AssetPicture model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(AssetPicture)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AssetPicture, {
            title: 'NewAssetPicture',
            exclude: ['PictureID'],
          }),
        },
      },
    })
    assetPicture: Omit<AssetPicture, 'PictureID'>,
  ): Promise<AssetPicture> {
    return this.assetPictureRepository.create(assetPicture);
  }

  @get('/asset-pictures/count', {
    responses: {
      '200': {
        description: 'AssetPicture model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(AssetPicture))
    where?: Where<AssetPicture>,
  ): Promise<Count> {
    return this.assetPictureRepository.count(where);
  }

  @get('/asset-pictures', {
    responses: {
      '200': {
        description: 'Array of AssetPicture model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(AssetPicture, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(AssetPicture))
    filter?: Filter<AssetPicture>,
  ): Promise<AssetPicture[]> {
    return this.assetPictureRepository.find(filter);
  }

  @patch('/asset-pictures', {
    responses: {
      '200': {
        description: 'AssetPicture PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AssetPicture, {partial: true}),
        },
      },
    })
    assetPicture: AssetPicture,
    @param.query.object('where', getWhereSchemaFor(AssetPicture))
    where?: Where<AssetPicture>,
  ): Promise<Count> {
    return this.assetPictureRepository.updateAll(assetPicture, where);
  }

  @get('/asset-pictures/{id}', {
    responses: {
      '200': {
        description: 'AssetPicture model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(AssetPicture, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(AssetPicture))
    filter?: Filter<AssetPicture>,
  ): Promise<AssetPicture> {
    return this.assetPictureRepository.findById(id, filter);
  }

  @patch('/asset-pictures/{id}', {
    responses: {
      '204': {
        description: 'AssetPicture PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AssetPicture, {partial: true}),
        },
      },
    })
    assetPicture: AssetPicture,
  ): Promise<void> {
    await this.assetPictureRepository.updateById(id, assetPicture);
  }

  @put('/asset-pictures/{id}', {
    responses: {
      '204': {
        description: 'AssetPicture PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() assetPicture: AssetPicture,
  ): Promise<void> {
    await this.assetPictureRepository.replaceById(id, assetPicture);
  }

  @del('/asset-pictures/{id}', {
    responses: {
      '204': {
        description: 'AssetPicture DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    try{
      let uploads = await this.assetUploadsRepository.findById(id);
      if(!uploads) throw new HttpErrors.BadRequest(`Upload not found`);
      await this.assetUploadsRepository.deleteById(id);
      await this.s3ImageUploadService.deleteFileS3(uploads.URL);
    }catch(error){
      console.log(error)
      throw new HttpErrors.BadRequest(`Something went wrong. Please try again.`);
    }
  }
}
