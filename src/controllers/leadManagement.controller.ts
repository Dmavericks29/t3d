import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  filterTemplate,
  deduplicate,
} from '@loopback/repository';
import {
  post,
  Request,
  requestBody,
  Response,
  RestBindings,
}from '@loopback/rest';
import multer from 'multer';
import { fstat } from 'fs';
import { AnyType, ANY } from '@loopback/repository';
import {LeadsRepository} from '../repositories/leads.repository';
import {Leads} from '../models/leads.model';
export class LeadManagementController{

  constructor(
    @repository(LeadsRepository)
    public leadsRepository: LeadsRepository,){

  }

  private fs = require('fs');
@post('/files', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: 'Files and fields',
      },
    },
  })
  async fileUpload(
    @requestBody({
        content: {
          'multipart/form-data': {
            'x-parser': 'stream',
            schema: {
              type: 'object',
            },
          },
        },
      })
      request: Request,
      @inject(RestBindings.Http.RESPONSE) response: Response,
    ): Promise<any> {
      const upload = multer({dest: './public/uploads/'});
      return new Promise<object>((resolve, reject) => {
        upload.any()(request, response, async (err: any) => {
          if (err) {
            reject(err);
          } 
                else 
                {
                  
                  let file: any = request.files;
                  let that = this;
                  for (let index = 0; index < file.length; index++) {
                    let element = file[index];
                    //console.log(element.fieldname);
                    this.fs.readFile(element.path,function(err:any,data:any)
                    {
                        if(err)
                        {
                            return console.error(err);
                        }
                        let total_data = JSON.parse(data);
                      // console.log(total_data);
                       for(let i=0;i<total_data.length;i++){
                         
                          let leadData = new Leads({
                            FirstName: total_data[i].FirstName,
                            LastName: total_data[i].LastName,
                            Website: total_data[i].Website,
                            Email: total_data[i].Email,
                            Phone: total_data[i].Phone,
                            Mobile: total_data[i].Mobile,
                            CompanyName: total_data[i].CompanyName,
                          
                          });
                          console.log(total_data);
                          that.leadsRepository.save(leadData);
                       }
                    })
                  }
                    //console.log(request.files)
                    resolve({status:true});
                }
            });
        });
    }
}