import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  Request,
  RestBindings,
  Response,
  HttpErrors,
} from '@loopback/rest';
import {Asset, AssetMarking} from '../models';
import {
  AssetRepository,
  AssetPictureRepository,
  CategoryRepository,
  ClientRepository,
  AssetBundleRepository
} from '../repositories';
import {inject} from '@loopback/core';
import multer from 'multer';
import {S3ImageUploadService} from '../services/s-3-image-upload.service';
import {ImageUploadServiceBindings} from '../keys';
import _ from 'lodash';
import {authenticate} from '@loopback/authentication';
import {SecurityBindings} from '@loopback/security';
import {MyUserProfile} from './specs/user-controller.specs';

@authenticate('jwt')
export class AssetController {
  constructor(
    @repository(AssetRepository)
    public assetRepository: AssetRepository,
    @repository(AssetPictureRepository)
    public assetPictureRepository: AssetPictureRepository,
    @inject(ImageUploadServiceBindings.S3_IMG_UPLOAD)
    private s3ImageUploadService: S3ImageUploadService,
    @repository(CategoryRepository)
    private categoryRepository: CategoryRepository,
    @repository(ClientRepository)
    private clientRepository: ClientRepository,
    @repository(AssetBundleRepository)
    private assetBundleRepository : AssetBundleRepository,
    @inject(SecurityBindings.USER)
    private currentUser: MyUserProfile,
  ) {}

  @post('/assets', {
    responses: {
      '200': {
        description: 'Asset model instance',
        content: {'application/json': {schema: getModelSchemaRef(Asset)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'multipart/form-data': {
          'x-parser': 'stream',
          schema: {
            type: 'object',
          },
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<any> {
    const upload = multer({dest: './public/uploads/'});
    return new Promise<object>((resolve, reject) => {
      upload.any()(request, response, async (err: any) => {
        if (err) {
          reject(err);
        } else {
          try {
            let assetData = request.body;
            let {
                AssetID,
                ClientID,
                CategoryID,
                Name,
                NoOfFeatures,
                UploadCount,
              } = request.body,
              asset:any,
              imageURL: any,
              zipURL: any;
              
            if (!AssetID) {
              assetData.Status = 'under-review';
              asset = await this.assetRepository.create(
                _.omit(assetData, ['Upload', 'AssetBundle']),
              );
            } else {
              await this.assetRepository.updateById(AssetID, {
                ClientID,
                CategoryID,
                Name,
                NoOfFeatures,
                UploadCount,
              });
              asset = new Asset({
                AssetID,
                ClientID,
                CategoryID,
                Name,
                NoOfFeatures,
                UploadCount,
              });
            }
            let ufile: any = request.files;
            let bundlePromise=[];
            for (let index = 0; index < ufile.length; index++) {
              let ClientName = (
                await this.assetRepository.client(asset.AssetID)
              ).FullName;
              ClientName = ClientName.replace(/[^\w\s]/gi, '')
                .split(' ')
                .join('_');
              let AssetName = asset.Name.replace(/[^\w\s]/gi, '')
                .split(' ')
                .join('_');

              const element = ufile[index];
              
              switch (element.fieldname) {
                case 'AssetBundle':

                  bundlePromise.push(await this.s3ImageUploadService.uploadFileToS3(
                    element,
                    {path: `${ClientName}/${AssetName}`, type:element.mimetype, name:`ProductImage_${+new Date}`},
                  ));
                  break;

                case 'Upload':
                  imageURL = await this.s3ImageUploadService.uploadFileToS3(
                    element,
                    {
                      path: `${ClientName}/${AssetName}`,
                      type: element.mimetype,
                      name: `Thumbnail`,
                    },
                  );
                  break;
                default:
                  break;
              }
            }
            let picData = await this.assetPictureRepository.findOne({
              where: {AssetID: asset.AssetID},
            });
            
            zipURL = await Promise.all(bundlePromise);
            if (zipURL.length>0) {
              zipURL = _.map(zipURL, item => { return {URL: item.Key,AssetID: asset.AssetID} });
            }

            let uploadData = [...zipURL];
            let bundleUploads = await this.assetBundleRepository.createAll(uploadData);

            uploadData = _.map(bundleUploads, async item=>{
              return {URL:await this.s3ImageUploadService.getSignedURL(item.URL), Type:item.Type};
            });

            if (!picData) {
              await this.assetPictureRepository.create({
                AssetID: asset.AssetID,
                URL: imageURL.Key,
                AssetBundle:'test.png' //zipURL.Key,
              });
            } else {
              if (imageURL) picData.URL = imageURL.Key;
              if (zipURL) picData.AssetBundle = 'test.png'
              picData = await this.assetPictureRepository.save(picData);
            }
            resolve({asset,uploadData});
          } catch (error) {
            console.log(error);
            reject(error);
          }
        }
      });
    });
    // return this.assetRepository.create(asset);
  }

  @get('/assets/count', {
    responses: {
      '200': {
        description: 'Asset model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Asset)) where?: Where<Asset>,
  ): Promise<Count> {
    console.log('asset count');
    if(this.currentUser.clientID){
      return this.assetRepository.count({ClientID:this.currentUser.clientID});
    }else{
      return this.assetRepository.count(where);
    }
    
  }

  @get('/assets/live', {
    responses: {
      '200': {
        description: 'Live Assets',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  @authenticate('jwt')
  async assetsLive(
   
    @param.query.object('where', getWhereSchemaFor(Asset))
    where?: Where<Asset>,
  ): Promise<Count> {
    // return this.clientRepository.dataSource.execute(`SELECT  COUNT(*) as total, month(CreatedAt) as month FROM Client GROUP BY month(CreatedAt)`);
    
    if(this.currentUser.clientID){
      let query = "SELECT COUNT(*) as total, monthname(CreatedAt) as month FROM Asset WHERE Status='published' AND ClientID ='"+this.currentUser.clientID+"' GROUP BY monthname(CreatedAt) ORDER BY CreatedAt;";      
      return this.assetRepository.dataSource.execute(query);

    }else{

      return this.assetRepository.dataSource.execute(`
      SELECT COUNT(*) as total, monthname(CreatedAt) as month FROM Asset WHERE Status='published' GROUP BY monthname(CreatedAt) ORDER BY CreatedAt;
    `);

    }
   

  }

  @get('/assets/bymonth', {
    responses: {
      '200': {
        description: 'Total Assets',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  @authenticate('jwt')
  async assetsTotal(
    @param.query.object('where', getWhereSchemaFor(Asset))
    where?: Where<Asset>,
  ): Promise<any> {
    // return this.clientRepository.dataSource.execute(`SELECT  COUNT(*) as total, month(CreatedAt) as month FROM Client GROUP BY month(CreatedAt)`);
    let all = await this.assetRepository.dataSource.execute(`
    SELECT 
      day(A.CreatedAt) as _date, dayname(A.CreatedAt) as _day, COUNT(B.CreatedAt) as count FROM
    (
      SELECT CreatedAt
      FROM
      (
        SELECT
          MAKEDATE(YEAR(NOW()),1) +
          INTERVAL (MONTH(NOW())-1) MONTH +
          INTERVAL daynum DAY CreatedAt
        FROM
        (
          SELECT t*10+u daynum
          FROM
            (SELECT 0 t UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) A,
            (SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3
            UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
            UNION SELECT 8 UNION SELECT 9) B
          ORDER BY daynum
        ) AA
      ) AAA
      WHERE MONTH(CreatedAt) = MONTH(NOW())
    ) A 
        LEFT JOIN trickdb_dev.Asset B on date(B.CreatedAt) = date(A.CreatedAt) GROUP BY A.CreatedAt
    `);

    let published = await this.assetRepository.dataSource.execute(`
    SELECT 
      day(A.UpdatedAt) as _date, dayname(A.UpdatedAt) as _day, COUNT(B.UpdatedAt) as count FROM
    (
      SELECT UpdatedAt
      FROM
      (
        SELECT
          MAKEDATE(YEAR(NOW()),1) +
          INTERVAL (MONTH(NOW())-1) MONTH +
          INTERVAL daynum DAY UpdatedAt
        FROM
        (
          SELECT t*10+u daynum
          FROM
            (SELECT 0 t UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) A,
            (SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3
            UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
            UNION SELECT 8 UNION SELECT 9) B
          ORDER BY daynum
        ) AA
      ) AAA
      WHERE MONTH(UpdatedAt) = MONTH(NOW())
    ) A 
        LEFT JOIN trickdb_dev.Asset B on date(B.UpdatedAt) = date(A.UpdatedAt) WHERE B.Status='published' GROUP BY A.UpdatedAt
    `);

    let unpublished = await this.assetRepository.dataSource.execute(`
    SELECT 
      day(A.UpdatedAt) as _date, dayname(A.UpdatedAt) as _day, COUNT(B.UpdatedAt) as count FROM
    (
      SELECT UpdatedAt
      FROM
      (
        SELECT
          MAKEDATE(YEAR(NOW()),1) +
          INTERVAL (MONTH(NOW())-1) MONTH +
          INTERVAL daynum DAY UpdatedAt
        FROM
        (
          SELECT t*10+u daynum
          FROM
            (SELECT 0 t UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) A,
            (SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3
            UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
            UNION SELECT 8 UNION SELECT 9) B
          ORDER BY daynum
        ) AA
      ) AAA
      WHERE MONTH(UpdatedAt) = MONTH(NOW())
    ) A 
        LEFT JOIN trickdb_dev.Asset B on date(B.UpdatedAt) = date(A.UpdatedAt) where B.Status = 'unpublished' GROUP BY A.UpdatedAt
    `);

    return {
      all,
      published,
      unpublished
    }

  }


  @get('/assets', {
    responses: {
      '200': {
        description: 'Array of Asset model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Asset, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @inject(SecurityBindings.USER)
    currentUserProfile: MyUserProfile,
    @param.query.object('filter', getFilterSchemaFor(Asset))
    filter?: Filter<Asset>,
  ): Promise<Asset[]> {
    let finaldata: Array<Asset> = [];
    let asset;
    let includeFilter = {
      include: [
        {
          relation: 'category',
          where: {Status: true},
        },
        {
          relation: 'client',
         
        },
        {
          relation: 'assetPicture',
        },
        {
          relation: 'assetMarking',
          scope: {
            include: [{relation: 'assetMarkingUploads'}],
          },
        },
      ],
      order: ['CreatedAt DESC'],
    };
    if (currentUserProfile.clientID) {
         includeFilter = {
          include: [
            {
              relation: 'category',
              where: {Status: true},
            },
            {
              relation: 'assetPicture',
            },
            {
              relation: 'assetMarking',
              scope: {
                include: [{relation: 'assetMarkingUploads'}],
              },
            },
          ],
          order: ['CreatedAt DESC'],
        };
    }
    asset = await this.assetRepository.find(includeFilter);
    if (currentUserProfile.clientID) {
      asset = await this.clientRepository
        .assets(currentUserProfile.clientID)
        .find(includeFilter);
    }
    for (let index = 0; index < asset.length; index++) {
      console.log(asset[index]);
      const element: any = Object.assign({}, asset[index]);
      if (element.assetPicture && element.assetPicture.URL)
        element.Upload = this.s3ImageUploadService.getSignedURL(
          element.assetPicture.URL,
        );
      if (element.assetPicture && element.assetPicture.AssetBundle) {
        let bundle = element.assetPicture.AssetBundle.split('/');
        element.AssetBundle = bundle[bundle.length - 1];
      }
      element.assetPicture = _.pick(element.assetPicture, ['PictureID']);
      element.assetBundle  = _.pick(element.assetBundle, ['BundleID']);
      element.category = _.pick(element.category, ['CategoryID', 'Name']);
      let temp = Object.assign({}, element);
      finaldata.push(temp);
    }
    return finaldata;
  }

  @patch('/assets', {
    responses: {
      '200': {
        description: 'Asset PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Asset, {partial: true}),
        },
      },
    })
    asset: Asset,
    @param.query.object('where', getWhereSchemaFor(Asset)) where?: Where<Asset>,
  ): Promise<Count> {
    return this.assetRepository.updateAll(asset, where);
  }

  @get('/assets/{id}', {
    responses: {
      '200': {
        description: 'Asset model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Asset))
    filter?: Filter<Asset>,
  ): Promise<Asset> {
    let temp: any;
    let asset = await this.assetRepository.findOne({
      where: {AssetID: id},
      include: [
        {
          relation: 'category',
        },
        {
          relation: 'assetPicture',
          scope: {
            include: [{relation: 'assetUploads'}],
          },
        },
        {
          relation: 'assetMarking',
          scope: {
            include: [{relation: 'assetMarkingUploads'}],
          },
        },
      ],
    });
    if (!asset) {
      throw new HttpErrors.BadRequest('No asset found.');
    }
    temp = Object.assign({}, asset);
    temp.assetPicture.URL = temp.assetPicture.URL
      ? this.s3ImageUploadService.getSignedURL(temp.assetPicture.URL)
      : null;
    temp.assetPicture.ProductVideoURL = temp.assetPicture.ProductVideoURL
      ? temp.assetPicture.ProductVideoURL
      : null;

    temp.assetPicture.assetUploads = _.map(
      temp.assetPicture.assetUploads,
      x => {
        return _.assign(x, {
          URL: x.URL ? this.s3ImageUploadService.getSignedURL(x.URL) : null,
        });
      },
    );
    
    let self = this;
    temp.assetMarking = _.map(temp.assetMarking, nested => {
      if (nested.assetMarkingUploads && nested.assetMarkingUploads.length > 0) {
        nested.assetMarkingUploads = _.map(
          nested.assetMarkingUploads,
          element => {
            element.URL = self.s3ImageUploadService.getSignedURL(element.URL);
            return element;
          },
        );
      }
      return nested;
    });
    
    let assetBundle = await this.assetBundleRepository.find({
      where: {AssetID: id},     
    });
    temp.assetBundle = assetBundle;
    return temp;
  }

  @get('/assets/{id}/bundle', {
    responses: {
      '200': {
        description: 'Asset model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Asset, {includeRelations: true}),
          },
        },
      },
    },
  })
  async bundle(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Asset))
    filter?: Filter<Asset>,
  ): Promise<Asset> {
    let temp: any;
    let asset = await this.assetRepository.findOne({
      where: { AssetID: id, Status: 'published' },
      // fields:{ AssetID:true, Name:true, NoOfFeatures:true, UploadCount:true },
      include: [
        {
          relation: 'category',
          // scope: {
          //   fields:{ CategoryID:true, Name:true }
          // }
        },
        {
          relation: 'assetPicture',
          scope: {
            // fields:{ PictureID:true, URL:true, AssetBundle:true },
            include: [
              {
                relation: 'assetUploads',
                // scope: {
                //   fields:{ AssetUploadID: true, URL:true, Type:true }
                // }
              }
            ],
          },
        },
        {
          relation: 'assetMarking',
          scope: {
            // fields:{ MarkingID:true, Title:true, Description:true },
            include: [
              {
                relation: 'assetMarkingUploads',
                // scope: {
                //   fields:{ AssetMarkingUploadID: true, URL:true, Type:true }
                // }
              }
            ],
          },
        },
      ],
    });
    if (!asset) {
      throw new HttpErrors.BadRequest('No asset found.');
    }
    temp = Object.assign({}, asset);
    temp.assetPicture.URL = temp.assetPicture.URL
      ? this.s3ImageUploadService.getSignedURL(temp.assetPicture.URL)
      : null;
    temp.assetPicture.ProductVideoURL = temp.assetPicture.ProductVideoURL
      ? temp.assetPicture.ProductVideoURL
      : null;

    temp.assetPicture.assetUploads = _.map(
      temp.assetPicture.assetUploads,
      x => {
        return _.assign(x, {
          URL: x.URL ? this.s3ImageUploadService.getSignedURL(x.URL) : null,
        });
      },
    );
    
    let self = this;
    temp.assetMarking = _.map(temp.assetMarking, nested => {
      if (nested.assetMarkingUploads && nested.assetMarkingUploads.length > 0) {
        nested.assetMarkingUploads = _.map(
          nested.assetMarkingUploads,
          element => {
            element.URL = self.s3ImageUploadService.getSignedURL(element.URL);
            return element;
          },
        );
      }
      return nested;
    });

    return temp;
  }

  @patch('/assets/{id}')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Asset, {partial: true}),
        },
      },
    })
    asset: Asset,
  ): Promise<any> {
    try {
      asset.UpdatedAt = new Date().toISOString();
      await this.assetRepository.updateById(id, asset);
      return this.findById(id);
    } catch (err) {
      console.log(err);
      throw new HttpErrors.InternalServerError(err);
    }
  }

  @post('/assets/status/{id}')
  async statusChange(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Asset, {partial: true}),
        },
      },
    })
    asset: Asset,
  ): Promise<any> {
    let data = await this.assetRepository.updateById(id, asset);
    this.findById(id);
  }

  @put('/assets/{id}', {
    responses: {
      '204': {
        description: 'Asset PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() asset: Asset,
  ): Promise<void> {
    await this.assetRepository.replaceById(id, asset);
  }

  @del('/assets/{id}', {
    responses: {
      '204': {
        description: 'Asset DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.assetRepository.deleteById(id);
  }

  @del('/assetbundle/{id}', {
    responses: {
      '204': {
        description: 'Bundle DELETE Success',
      },
    },
  })
  async bundledeleteById(@param.path.string('id') id: string): Promise<void> {
    // await this.assetMarkingRepository.deleteById(id);
    try{
      let bundleUploads = await this.assetBundleRepository.findById(id);
      if(!bundleUploads) throw new HttpErrors.BadRequest(`Bundle upload not found`);
      await this.assetBundleRepository.deleteById(id);
      await this.s3ImageUploadService.deleteFileS3(bundleUploads.URL);
    }catch(error){
      console.log(error)
      throw new HttpErrors.BadRequest(`Something went wrong. Please try again.`);
    }
  }

  @get('/assets/{id}/assetMarking')
  async createOrder(
    @param.path.string('id') AssetID: typeof Asset.prototype.AssetID,
  ): Promise<AssetMarking[]> {
    return await this.assetRepository.assetMarking(AssetID).find();
  }
}
