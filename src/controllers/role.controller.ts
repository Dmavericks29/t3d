import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Role } from '../models';
import { RoleRepository } from '../repositories/role.repository';
import { authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { SecurityBindings } from '../keys';
import { inject } from '@loopback/core';
import { MyUserProfile } from './specs/user-controller.specs';
import * as _ from 'lodash'

@authenticate('jwt')
export class RoleController {
  constructor(
    @repository(RoleRepository)
    public roleRepository: RoleRepository,
    @inject(SecurityBindings.USER)
    private currentUserProfile: MyUserProfile,
  ) { }

  @post('/roles', {
    responses: {
      '200': {
        description: 'Role model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Role) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Role, {
            title: 'NewRole',
            exclude: ['RoleID'],
          }),
        },
      },
    })
    role: Omit<Role, 'RoleID'>,
  ): Promise<Role> {
    return this.roleRepository.create(role);
  }

  @get('/roles/count', {
    responses: {
      '200': {
        description: 'Role model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Role)) where?: Where<Role>,
  ): Promise<Count> {
    return this.roleRepository.count(where);
  }

  @get('/roles', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Role model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              // items: getModelSchemaRef(Role, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  @authorize({ resource: 'roles', scopes: ['view'] })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Role)) filter?: Filter<Role>,
  ): Promise<any> {
    let {
      role,
    } = this.currentUserProfile,
    adminRoles =[ 1,2,3,4,5],
    clientRoles = [2,4];

    let roles =  await this.roleRepository.find({
      where:{
        Status:true
      },
      fields:{
        "CreatedAt": false,
        "UpdatedAt": false
      },
      order:['Name ASC']
    });

    if(role==1){
      return _.filter(roles, (item:Role) => {
        if(adminRoles.includes(item.RoleID)) return item;
      });
    }else if(role==2){
      return _.filter(roles, (item:Role) => {
        if(clientRoles.includes(item.RoleID)) return item;
      });
    }
  }

  @patch('/roles', {
    responses: {
      '200': {
        description: 'Role PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Role, { partial: true }),
        },
      },
    })
    role: Role,
    @param.query.object('where', getWhereSchemaFor(Role)) where?: Where<Role>,
  ): Promise<Count> {
    return this.roleRepository.updateAll(role, where);
  }

  @get('/roles/{id}', {
    responses: {
      '200': {
        description: 'Role model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Role, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: number,
    @param.query.object('filter', getFilterSchemaFor(Role)) filter?: Filter<Role>
  ): Promise<Role> {
    return this.roleRepository.findById(id, filter);
  }

  @patch('/roles/{id}', {
    responses: {
      '204': {
        description: 'Role PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Role, { partial: true }),
        },
      },
    })
    role: Role,
  ): Promise<void> {
    await this.roleRepository.updateById(id, role);
  }

  @put('/roles/{id}', {
    responses: {
      '204': {
        description: 'Role PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: number,
    @requestBody() role: Role,
  ): Promise<void> {
    await this.roleRepository.replaceById(id, role);
  }

  @del('/roles/{id}', {
    responses: {
      '204': {
        description: 'Role DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: number): Promise<void> {
    await this.roleRepository.deleteById(id);
  }
}
