import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {UserLogs} from '../models';
import {UserLogsRepository} from '../repositories';

export class UserlogsController {
  constructor(
    @repository(UserLogsRepository)
    public userLogsRepository : UserLogsRepository,
  ) {}

  @post('/user-logs', {
    responses: {
      '200': {
        description: 'UserLogs model instance',
        content: {'application/json': {schema: getModelSchemaRef(UserLogs)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserLogs, {
            title: 'NewUserLogs',
            exclude: ['LogID'],
          }),
        },
      },
    })
    userLogs: Omit<UserLogs, 'LogID'>,
  ): Promise<UserLogs> {
    return this.userLogsRepository.create(userLogs);
  }

  @get('/user-logs/count', {
    responses: {
      '200': {
        description: 'UserLogs model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(UserLogs)) where?: Where<UserLogs>,
  ): Promise<Count> {
    return this.userLogsRepository.count(where);
  }

  @get('/user-logs', {
    responses: {
      '200': {
        description: 'Array of UserLogs model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(UserLogs, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(UserLogs)) filter?: Filter<UserLogs>,
  ): Promise<UserLogs[]> {
    return this.userLogsRepository.find(filter);
  }

  @patch('/user-logs', {
    responses: {
      '200': {
        description: 'UserLogs PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserLogs, {partial: true}),
        },
      },
    })
    userLogs: UserLogs,
    @param.query.object('where', getWhereSchemaFor(UserLogs)) where?: Where<UserLogs>,
  ): Promise<Count> {
    return this.userLogsRepository.updateAll(userLogs, where);
  }

  @get('/user-logs/{id}', {
    responses: {
      '200': {
        description: 'UserLogs model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(UserLogs, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(UserLogs)) filter?: Filter<UserLogs>
  ): Promise<UserLogs> {
    return this.userLogsRepository.findById(id, filter);
  }

  @patch('/user-logs/{id}', {
    responses: {
      '204': {
        description: 'UserLogs PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserLogs, {partial: true}),
        },
      },
    })
    userLogs: UserLogs,
  ): Promise<void> {
    await this.userLogsRepository.updateById(id, userLogs);
  }

  @put('/user-logs/{id}', {
    responses: {
      '204': {
        description: 'UserLogs PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() userLogs: UserLogs,
  ): Promise<void> {
    await this.userLogsRepository.replaceById(id, userLogs);
  }

  @del('/user-logs/{id}', {
    responses: {
      '204': {
        description: 'UserLogs DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.userLogsRepository.deleteById(id);
  }
}
