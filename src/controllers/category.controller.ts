import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
} from '@loopback/rest';
import { Category, Client } from '../models';
import { CategoryRepository, UserRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { SecurityBindings } from '@loopback/security';
import { MyUserProfile } from './specs/user-controller.specs';
import { authorize } from '@loopback/authorization';

@authenticate('jwt')
// @authorize({ resource: 'category', scopes: ['find', 'create', 'edit', 'view', 'patch'] })
export class CategoryController {
  constructor(
    @repository(CategoryRepository)
    public categoryRepository: CategoryRepository,
    @repository(UserRepository) 
    private userRepository:UserRepository
  ) { }

  @post('/categories', {
    responses: {
      '200': {
        description: 'Category model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Category) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Category, {
            title: 'NewCategory',
            exclude: ['CategoryID'],
          }),
        },
      },
    })
    category: Omit<Category, 'CategoryID'>,
    @inject(SecurityBindings.USER)
    currentUserProfile: MyUserProfile,
  ): Promise<Category> {
    category.CreatedBy = currentUserProfile.id;
    let user = await this.userRepository.findById(currentUserProfile.id);
    let _category = await this.categoryRepository.find({where:{ ClientID:user.ClientID, Name : category.Name}});
    if(_category.length>0){
      throw new HttpErrors.BadRequest('Category Name already exist');
    }
    category.ClientID = user.ClientID;
    return this.categoryRepository.create(category);
  }

  @get('/categories/{id}/client')
  async getClient(
    @param.path.string('id') id: typeof Category.prototype.CategoryID,
  ): Promise<Client> {
    return this.categoryRepository.client(id);
  }

  @get('/categories/client/{id}')
  async getCategoryByClient(
    @param.path.string('id') ClientID: typeof Client.prototype.ClientID,
  ): Promise<Category[]> {
    return await this.categoryRepository.find({where:{ClientID, Status:true}});
  }

  @get('/categories/count', {
    responses: {
      '200': {
        description: 'Category model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Category)) where?: Where<Category>,
  ): Promise<Count> {
    return this.categoryRepository.count(where);
  }


  
  @get('categories/client_categories_count/{id}', {
    responses: {
      '200': {
        description: 'Category model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async clientCatCount(
    @param.path.string('id') id: typeof Client.prototype.ClientID,    
  ): Promise<Count> {
    console.log('category count');
    console.log(id);
    return this.categoryRepository.count({ClientID:id});
  }
  
  @get('/categories', {
    responses: {
      '200': {
        description: 'Array of Category model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Category, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Category)) filter?: Filter<Category>,
  ): Promise<Category[]> {
    return this.categoryRepository.find(filter);
  }

  @get('/categories/parent')
  async getParents(): Promise<Category[]> {
    return this.categoryRepository.find({
      where: {
        Parent: ""
      },
      fields: { CategoryID: true, Name: true }
    });
  }

  @patch('/categories', {
    responses: {
      '200': {
        description: 'Category PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Category, { partial: true }),
        },
      },
    })
    category: Category,
    @param.query.object('where', getWhereSchemaFor(Category)) where?: Where<Category>,
  ): Promise<Count> {
    return this.categoryRepository.updateAll(category, where);
  }

  @get('/categories/{id}', {
    responses: {
      '200': {
        description: 'Category model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Category, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Category)) filter?: Filter<Category>
  ): Promise<Category> {
    return this.categoryRepository.findById(id, filter);
  }

  @patch('/categories/{id}', {
    responses: {
      '204': {
        description: 'Category PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Category, { partial: true }),
        },
      },
    })
    category: Category,
  ): Promise<void> {
    await this.categoryRepository.updateById(id, category);
  }

  @put('/categories/{id}', {
    responses: {
      '204': {
        description: 'Category PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() category: Category,
  ): Promise<void> {
    await this.categoryRepository.replaceById(id, category);
  }

  @del('/categories/{id}', {
    responses: {
      '204': {
        description: 'Category DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.categoryRepository.deleteById(id);
  }
}
