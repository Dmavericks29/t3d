import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  filterTemplate,
  deduplicate,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Client, Userdata, User} from '../models';
import {ClientRepository, UserRepository,CategoryRepository} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';
import {inject} from '@loopback/core';
import {UserControllerBindings} from '../keys';
import {SecurityBindings, securityId} from '@loopback/security';
import {UserController} from './user.controller';
import * as _ from 'lodash';
import { notEqual } from 'assert';
import { MyUserProfile } from './specs/user-controller.specs';

@authenticate('jwt')
export class ClientController {
  constructor(
    @repository(ClientRepository)
    public clientRepository: ClientRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(UserControllerBindings.USER_CTRL)
    private userController: UserController,
    @inject(SecurityBindings.USER)
    private currentUserProfile: MyUserProfile,
    @repository(CategoryRepository)
    public categoryRepository: CategoryRepository,
  ) {}

  @post('/clients', {
    responses: {
      '200': {
        description: 'Client model instance',
        content: {'application/json': {schema: getModelSchemaRef(Client)}},
      },
    },
  })
  @authenticate('jwt')
  @authorize({resource: 'clients', scopes: ['create']})
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Client, {
            title: 'NewClient',
            exclude: ['ClientID'],
          }),
        },
      },
    })
    client:Omit<Client, 'ClientID'>,
  ): Promise<Client> {
    let userData = new Userdata({
      Email: client.Email,
      UserName: client.Email,
      LastName: client.LastName,
      FirstName: _.startCase(client.FullName),
      Type: 'client',
      Password: '',
      RoleID: 2,
    });
    try {
      client.FirstName = _.startCase(client.FullName);
      let savedUser = await this.userController.create(userData);
      let savedClient = await this.clientRepository.create(client);
      savedUser.ClientID = savedClient.ClientID;
      await this.userRepository.save(savedUser);
      return await this.clientRepository.save(savedClient);
    } catch (err) {
      throw err;
    }
  }

  @get('/clients/count', {
    responses: {
      '200': {
        description: 'Client model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  @authenticate('jwt')
  async count(
    @param.query.object('where', getWhereSchemaFor(Client))
    where?: Where<Client>,
  ): Promise<Count> {
    return this.clientRepository.count(where);
  }

  @get('/clients/live', {
    responses: {
      '200': {
        description: 'Client model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  @authenticate('jwt')
  async clientsLive(
    @param.query.object('where', getWhereSchemaFor(Client))
    where?: Where<Client>,
  ): Promise<Count> {
    // return this.clientRepository.dataSource.execute(`SELECT  COUNT(*) as total, month(CreatedAt) as month FROM Client GROUP BY month(CreatedAt)`);
    return this.clientRepository.dataSource.execute(`
      SELECT COUNT(*), monthname(CreatedAt) FROM trickdb_dev.Client GROUP BY monthname(CreatedAt) ORDER BY CreatedAt;
    `);
  }

  @get('/clients', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Client model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Client, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({resource: 'clients', scopes: ['find']})
  async find(
    @param.query.object('filter', getFilterSchemaFor(Client))
    filter?: Filter<Client>,
  ): Promise<Client[]> {
    return await this.clientRepository.find({include: [{relation: 'users'}]});
  }

  // @post('/clients/{id}/users')
  // async createOrder(
  //   @param.path.number('id') customerId: typeof Client.prototype.ClientID,
  //   @requestBody() orderData: User,
  // ): Promise<User> {
  //   return this.customerRepository.orders(customerId).create(orderData);
  // }

  @patch('/clients', {
    responses: {
      '200': {
        description: 'Client PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Client, {partial: true}),
        },
      },
    })
    client: Client,
    @param.query.object('where', getWhereSchemaFor(Client))
    where?: Where<Client>,
  ): Promise<Count> {
    return this.clientRepository.updateAll(client, where);
  }

  @get('/clients/{id}', {
    responses: {
      '200': {
        description: 'Client model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Client, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Client))
    filter?: Filter<Client>,
  ): Promise<Client> {
    return this.clientRepository.findById(id, filter);
  }

  @patch('/clients/{id}', {
    responses: {
      '204': {
        description: 'Client PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody() client: any,
  ): Promise<any> {

    let userData = new User({
      Email: client.Email,
      UserName: client.Email,
      FirstName: _.startCase(client.FullName),
    });
    try {

      

      let client_det = await this.clientRepository.find({where:{Email:client.Email}});      
      let filter_data = client_det.filter(function(item){
        return item.ClientID != client.ClientID
      });
      if(filter_data.length > 0){
        return {'status':false,'message':'Email Id already exist with other client, Please try another.'}
      }else{
        //let userdet = await this.userRepository.find({where:{'Email':client_det[0].Email}});        
        let savedUser = await this.userController.updateById(
          client.user.UserID,
          userData,
        );
        client.FirstName = _.startCase(client.FullName); 
       
        await this.clientRepository.updateById(id,client);
      }
    } catch (err) {
      throw err;
    }
  }

  
  @patch('/clients_status/{id}', {
    responses: {
      '204': {
        description: 'Client PATCH success',
      },
    },
  })
  async updateStatuById(
    @param.path.string('id') id: string,
    @requestBody() client: any,
  ): Promise<any> {

    let userData = new User({
      Email: client.Email,
      UserName: client.Email,
      FirstName: _.startCase(client.FullName),
    });
    try {

      await this.clientRepository.updateById(id,client);
    } catch (err) {
      throw err;
    }
  }


  @put('/clients/{id}', {
    responses: {
      '204': {
        description: 'Client PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() client: Client,
  ): Promise<void> {
    await this.clientRepository.replaceById(id, client);
  }

  @del('/clients/{id}', {
    responses: {
      '204': {
        description: 'Client DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.clientRepository.deleteById(id);
  }

  
  /* @get('/client_categories_count/{id}', {
    responses: {
      '200': {
        description: 'Category model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async catCount(
    @param.path.string('id') id: string,    
  ): Promise<Count> {
    console.log('category count');
    console.log(id);
    return this.categoryRepository.count({ClientID:id});
  } */
  
}
