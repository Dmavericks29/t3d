import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {UserType} from '../models';
import {UserTypeRepository} from '../repositories';
import { authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';

@authenticate('jwt')
export class UsertypesController {
  constructor(
    @repository(UserTypeRepository)
    public userTypeRepository : UserTypeRepository,
  ) {}

  @post('/usertypes', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'UserType model instance',
        content: {'application/json': {schema: getModelSchemaRef(UserType)}},
      },
    },
  })
  @authorize({ resource: 'users', scopes: ['create'] })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserType, {
            title: 'NewUserType',
            exclude: ['TypeID'],
          }),
        },
      },
    })
    userType: Omit<UserType, 'TypeID'>,
  ): Promise<UserType> {
    return this.userTypeRepository.create(userType);
  }

  @get('/usertypes/count', {
    responses: {
      '200': {
        description: 'UserType model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(UserType)) where?: Where<UserType>,
  ): Promise<Count> {
    return this.userTypeRepository.count(where);
  }

  @get('/usertypes', {
    responses: {
      '200': {
        description: 'Array of UserType model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(UserType, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(UserType)) filter?: Filter<UserType>,
  ): Promise<UserType[]> {
    return this.userTypeRepository.find(filter);
  }

  @patch('/usertypes', {
    responses: {
      '200': {
        description: 'UserType PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserType, {partial: true}),
        },
      },
    })
    userType: UserType,
    @param.query.object('where', getWhereSchemaFor(UserType)) where?: Where<UserType>,
  ): Promise<Count> {
    return this.userTypeRepository.updateAll(userType, where);
  }

  @get('/usertypes/{id}', {
    responses: {
      '200': {
        description: 'UserType model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(UserType, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.query.object('filter', getFilterSchemaFor(UserType)) filter?: Filter<UserType>
  ): Promise<UserType> {
    return this.userTypeRepository.findById(id, filter);
  }

  @patch('/usertypes/{id}', {
    responses: {
      '204': {
        description: 'UserType PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserType, {partial: true}),
        },
      },
    })
    userType: UserType,
  ): Promise<void> {
    await this.userTypeRepository.updateById(id, userType);
  }

  @put('/usertypes/{id}', {
    responses: {
      '204': {
        description: 'UserType PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() userType: UserType,
  ): Promise<void> {
    await this.userTypeRepository.replaceById(id, userType);
  }

  @del('/usertypes/{id}', {
    responses: {
      '204': {
        description: 'UserType DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userTypeRepository.deleteById(id);
  }
}
