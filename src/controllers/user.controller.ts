import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  model,
  property,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
  Request,
  RestBindings,
  Response,
} from '@loopback/rest';
import {User, UserTokens, Client} from '../models';
import {
  UserRepository,
  Credentials,
  AsClientCredentials,
  UserTokensRepository,
  ClientRepository,
  UserSessionsRepository,
} from '../repositories';
import {validateCredentials} from '../services/validator';
import * as _ from 'lodash';
import {PasswordHasher} from '../services/hash.password.bcryptjs';
import {
  PasswordHasherBindings,
  UserServiceBindings,
  TokenServiceBindings,
  MailServiceBindings,
  ImageUploadServiceBindings,
} from '../keys';
import {inject} from '@loopback/core';
import {
  CredentialsRequestBody,
  LoginAsClientRequestBody,
  UserProfileSchema,
  MyUserProfile,
} from './specs/user-controller.specs';
import {
  UserService,
  TokenService,
  authenticate,
} from '@loopback/authentication';
import {Userdata} from '../models/userdata.model';
import {authorize} from '@loopback/authorization';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';
import {SecurityBindings, securityId} from '@loopback/security';
import {MailerService, S3ImageUploadService} from '../services';
// import { compareId } from '../services/id.compare.authorizor';
import * as generator from 'generate-password';
import crypto from 'crypto';
import multer from 'multer';
import {JWTService} from '../services/jwt-service';

@model()
export class NewUserRequest extends User {
  @property({
    type: 'string',
    required: true,
  })
  Password: string;
}

@model()
export class ConfirmPasswords {
  @property({
    type: 'string',
    required: true,
  })
  Password: string;

  @property({
    type: 'string',
    required: true,
  })
  ConfirmPassword: string;
}

export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(TokenServiceBindings.JWT_SERVICE)
    public _jwtService: JWTService,
    @inject(SecurityBindings.USER)
    private currentUserProfile: MyUserProfile,
    @inject(MailServiceBindings.MAIL)
    private _mailService: MailerService,
    @repository(UserTokensRepository)
    public userTokensRepository: UserTokensRepository,
    @repository(UserSessionsRepository)
    public userSessionsRepository: UserSessionsRepository,
    @repository(ClientRepository)
    public clientRepository: ClientRepository,
    @inject(ImageUploadServiceBindings.S3_IMG_UPLOAD)
    private s3ImageUploadService: S3ImageUploadService,
  ) {}

  @post('/users', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({resource: 'users', scopes: ['create']})
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Userdata, {
            title: 'NewUser',
          }),
        },
      },
    })
    newUserRequest: Userdata,
  ): Promise<User> {
    newUserRequest.Password = generator.generate({
      length: 10,
      numbers: true,
      uppercase: true,
    });
    validateCredentials(_.pick(newUserRequest, ['Email', 'Password']));

    let adminTypes = [1, 3];
    let clientTypes = [2, 4, 5];
    try {
      newUserRequest.FirstName = _.startCase(newUserRequest.FirstName);
      newUserRequest.LastName = _.startCase(newUserRequest.LastName);
      if (clientTypes.includes(newUserRequest.RoleID)) {
        newUserRequest.Type = 'client';
      }
      if (adminTypes.includes(newUserRequest.RoleID)) {
        newUserRequest.Type = 'admin';
      }
      const savedUser = await this.userRepository.create(
        _.omit(newUserRequest, 'Password'),
      );
      savedUser.CreatedBy = this.currentUserProfile.id;
      
      if (newUserRequest.ClientID) {
        savedUser.ClientID = newUserRequest.ClientID;
        
        let client_det = await this.clientRepository.findOne({where:{'ClientID':newUserRequest.ClientID}});
        console.log(client_det)
        if(client_det != null){
          savedUser.ClientName = client_det.FirstName+" "+client_det.LastName;
          savedUser.ClientCompany = client_det.Company;
        }        
      }
      else{ 
        savedUser.ClientID = this.currentUserProfile.clientID;
        if(this.currentUserProfile.clientID != null && this.currentUserProfile.clientID != ""){
          let client_det = await this.clientRepository.findOne({where:{'ClientID':this.currentUserProfile.clientID}});
          console.log(client_det)
          if(client_det != null){
            savedUser.ClientName = client_det.FirstName+" "+client_det.LastName;
            savedUser.ClientCompany = client_det.Company;
          }

        }
        
      }

      await this.userRepository.save(savedUser);

      let token = await this.userTokensRepository.create(
        new UserTokens({
          UserID: savedUser.UserID,
          token: crypto
            .randomBytes(48)
            .toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/\=/g, ''),
        }),
      );
      await this._mailService.sendMail(
        {
          to: savedUser.Email,
          subject: 'Airshow.fun User Registration',
        },
        savedUser,
        token.token,
        token.id,
      );
      return savedUser;
    } catch (error) {
      if (error.statusCode === '422' && error.message.includes('uniqueEmail')) {
        throw new HttpErrors.Conflict('Email value is already taken');
      } else {
        throw error;
      }
    }
  }

  @post('/users/resend/link')
  @authenticate('jwt')
  @authorize({resource: 'users', scopes: ['resend']})
  async resendActivationLink(
    @requestBody({
      content: {
        'text/plain': {
          schema: {
            type: 'string',
          },
        },
      },
    })
    ClientID: string,
  ): Promise<any> {
    try {
      let user = await this.userRepository.findOne({
        where: {ClientID},
      });
      if (!user) throw new HttpErrors.BadRequest('User data not found');
      let tokenData = await this.userTokensRepository.findOne({
        where: {UserID: user.UserID},
      });
      if (tokenData) await this.userTokensRepository.deleteById(tokenData.id);
      let token = await this.userTokensRepository.create(
        new UserTokens({
          UserID: user?.UserID,
          token: crypto
            .randomBytes(48)
            .toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/\=/g, ''),
        }),
      );
      await this._mailService.sendMail(
        {
          to: user.Email,
          subject: 'Airshow.fun User Registration',
        },
        user,
        token.token,
        token.id,
      );
      return {error: false, message: 'Activation link sent.'};
    } catch (err) {
      throw err;
    }
  }


  @post('/users/resend/useractivation')
  @authenticate('jwt')
  @authorize({resource: 'users', scopes: ['resend']})
  async resendUserActivationLink(
    @requestBody({
      content: {
        'text/plain': {
          schema: {
            type: 'string',
          },
        },
      },
    })
    UserID: string,
  ): Promise<any> {
    try {
      let user = await this.userRepository.findOne({
        where: {UserID},
      });
      if (!user) throw new HttpErrors.BadRequest('User data not found');
      let tokenData = await this.userTokensRepository.findOne({
        where: {UserID: user.UserID},
      });
      if (tokenData) await this.userTokensRepository.deleteById(tokenData.id);
      let token = await this.userTokensRepository.create(
        new UserTokens({
          UserID: user?.UserID,
          token: crypto
            .randomBytes(48)
            .toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/\=/g, ''),
        }),
      );
      await this._mailService.sendMail(
        {
          to: user.Email,
          subject: 'Airshow.fun User Registration',
        },
        user,
        token.token,
        token.id,
      );
      return {error: false, message: 'Activation link sent.'};
    } catch (err) {
      throw err;
    }
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
                role: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<any> {
    const user = await this.userService.verifyCredentials(credentials);
    console.log(user)
    const userProfile = this.userService.convertToUserProfile(user);
    console.log(userProfile)
    const token = await this.jwtService.generateToken(userProfile);
    console.log(token)
    let session = await this.userSessionsRepository.findOne({
      where: {UserID: user.UserID},
    });
    if (!session)
      await this.userRepository.userSession(user.UserID).create({Token: token});
    else {
      // throw new HttpErrors.BadRequest(
      //   `User already loggedin in another device. `,
      // );
      return {
        error:true,
        message:`User already logged in another device.`,
        token: session.Token
      }
    }
    return {token};
  }

  @authenticate('jwt')
  @post('/users/logout')
  async logOut(
    @requestBody({
      description: 'The input of logout function',
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['token'],
            properties: {
              token: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    token: any,
  ): Promise<boolean> {
    let Token = token.token;
    let session = await this.userSessionsRepository.findOne({where: {Token}});
    if (!session) {
      throw new HttpErrors.BadRequest(`Token not exist.`);
    }
    let dest = this._jwtService.destroyToken(session.UserID);
    if (dest) {
      await this.userSessionsRepository.deleteById(session.SessionID);
    }
    return dest;
  }

  @post('/users/force/logout')
  async forceLogOut(
    @requestBody({
      description: 'Force logout function',
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['email'],
            properties: {
              email: {
                type: 'string',
                format: 'email'
              },
            },
          },
        },
      },
    })
    data: any,
  ): Promise<boolean> {
    let {
      email
    } = data;

    let user = await this.userRepository.findOne({where:{Email:email}});
    if(!user){
      throw new HttpErrors.BadRequest(`User not found`);
    }
    let session = await this.userSessionsRepository.findOne({where: {UserID:user.UserID}});
    if (!session) {
      throw new HttpErrors.BadRequest(`Token not exist.`);
    }
    let dest = this._jwtService.destroyToken(session.UserID);
    if (dest) {
      await this.userSessionsRepository.deleteById(session.SessionID);
    }
    return dest;
  }

  @post('/users/client/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
                role: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async loginAsClient(
    @requestBody(LoginAsClientRequestBody) email: AsClientCredentials,
  ): Promise<{token: string}> {
    let {Email} = email;
    const user = await this.userRepository.findOne({where: {Email}});
    if (!user) {
      throw new HttpErrors.BadRequest('User not found.');
    }
    if (!user.IsActive || !user.Status) {
      throw new HttpErrors.BadRequest('User not activated.');
    }
    let client = await this.clientRepository.findById(user.ClientID);
    if (!client.Status) {
      throw new HttpErrors.BadRequest(`Client is not active.`);
    }
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);
    return {token};
  }

  @get('/users/me', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'The current user profile',
        content: {
          'application/json': {
            schema: UserProfileSchema,
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async printCurrentUser(
    @inject(SecurityBindings.USER)
    currentUserProfile: MyUserProfile,
  ): Promise<User> {
    currentUserProfile.id = currentUserProfile[securityId];
    let profile = await this.userRepository.findById(currentUserProfile.id, {
      fields: {
        UserName: true,
        FirstName: true,
        LastName: true,
        Email: true,
        HeaderColor:true,
        FontColor:true,
        SidebarColor:true,
        Logo:true,
        ProfilePic: true,
      },
    });
    if (!profile) {
      throw new HttpErrors.BadRequest(`User not found`);
    }
    if (profile.ProfilePic)
      profile.ProfilePic = this.s3ImageUploadService.getSignedURL(
        profile.ProfilePic,
      );
    if (profile.Logo)
      profile.Logo = this.s3ImageUploadService.getSignedURL(
        profile.Logo,
      );
    return profile;
  }

  @get('/users/{id}/role')
  async getRole(
    @param.path.string('id') id: typeof User.prototype.UserID,
  ): Promise<string> {
    return (await this.userRepository.role(id)).Name;
  }

  @get('/users/count', {
    responses: {
      '200': {
        description: 'User model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.count(where);
  }

  @post('/users/password/{token}')
  async createPassword(
    @requestBody({
      description: 'Create passwords request data',
      content: {
        'application/x-www-form-urlencoded': {
          schema: {
            type: 'object',
            required: ['Password', 'ConfirmPassword'],
            properties: {
              Password: {
                type: 'string',
              },
              ConfirmPassword: {
                type: 'string',
              },
            },
            example: {
              Password: 'string',
              ConfirmPassword: 'string',
            },
          },
        },
      },
    })
    body: ConfirmPasswords,
    @param.path.string('token') token: string,
  ): Promise<any> {
    let {Password, ConfirmPassword} = body;

    if (Password.localeCompare(ConfirmPassword) != 0) {
      throw new HttpErrors.BadRequest(`Passwords mismatch.`);
    }
    try {
      let tokenData = await this.userTokensRepository.findOne({
        where: {
          token,
        },
      });
      if (!tokenData) throw new HttpErrors.BadRequest(`Invalid token.`);
      if (tokenData) {
        let userLogin = await this.userRepository
          .userLogin(tokenData.UserID)
          .create({Password: await this.passwordHasher.hashPassword(Password)});
        await this.userRepository.updateById(tokenData.UserID, {
          IsActive: true,
        });
        if (userLogin) {
          await this.userTokensRepository.deleteById(tokenData.id);
          return {
            statusCode: 200,
            message: 'Password created successfully.',
          };
        }
      }
    } catch (error) {
      throw new HttpErrors.BadRequest(`Something went wrong.`);
    }
  }

  @put('/users/password/{token}')
  async resetforgotPassword(
    @requestBody({
      description: 'Create passwords request data',
      content: {
        'application/x-www-form-urlencoded': {
          schema: {
            type: 'object',
            required: ['Password', 'ConfirmPassword'],
            properties: {
              Password: {
                type: 'string',
              },
              ConfirmPassword: {
                type: 'string',
              },
            },
            example: {
              Password: 'string',
              ConfirmPassword: 'string',
            },
          },
        },
      },
    })
    body: ConfirmPasswords,
    @param.path.string('token') token: string,
  ): Promise<any> {
    let {Password, ConfirmPassword} = body;

    if (Password.localeCompare(ConfirmPassword) != 0) {
      throw new HttpErrors.BadRequest(`Passwords mismatch.`);
    }
    try {
      let tokenData = await this.userTokensRepository.findOne({
        where: {
          token,
        },
      });
      if (!tokenData) throw new HttpErrors.BadRequest(`Invalid token.`);
      if (tokenData) {
        let userLogin = await this.userRepository
          .userLogin(tokenData.UserID)
          .patch({Password: await this.passwordHasher.hashPassword(Password)});
        await this.userRepository.updateById(tokenData.UserID, {
          IsActive: true,
        });
        if (userLogin) {
          await this.userTokensRepository.deleteById(tokenData.id);
          return {
            statusCode: 200,
            message: 'Password created successfully.',
          };
        }
      }
    } catch (error) {
      throw new HttpErrors.BadRequest(`Something went wrong.`);
    }
  }

  @post('/users/{id}/userToken')
  async createToken(
    @param.path.number('id') userId: typeof User.prototype.UserID,
    @requestBody() tokenData: UserTokens,
  ): Promise<UserTokens> {
    return this.userRepository.userToken(userId).create(tokenData);
  }

  @post('/users/reset/password')
  async resetPassword(
    @inject(SecurityBindings.USER)
    currentUserProfile: MyUserProfile,
    @requestBody({
      description: 'Reset password request data',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['CurrentPassword', 'Password', 'ConfirmPassword'],
            properties: {
              CurrentPassword: {
                type: 'string',
              },
              Password: {
                type: 'string',
              },
              ConfirmPassword: {
                type: 'string',
              },
            },
            example: {
              CurrentPassword: 'string',
              Password: 'string',
              ConfirmPassword: 'string',
            },
          },
        },
      },
    })
    req: Request,
  ): Promise<any> {
    console.log(currentUserProfile);
    console.log(req);

    let data: any = req;
    let credentials = {
      Email: currentUserProfile.email,
      Password: data.CurrentPassword,
    };

    const user = await this.userService.verifyCredentials(credentials);
    console.log(user);
    if (data.Password !== data.ConfirmPassword) {
      throw new HttpErrors.BadRequest(
        `Password and Confirm Password do not match.`,
      );
    }
    let ulogin = this.userRepository
      .userLogin(user.UserID)
      .patch({Password: await this.passwordHasher.hashPassword(data.Password)});
    console.log(ulogin);
    return {
      error: false,
      message: 'Password changed successfully',
    };
  }

  @post('/users/forgot')
  async forgotPassword(
    @requestBody({
      description: 'Reset password request data',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['Email'],
            properties: {
              Email: {
                type: 'string',
              },
            },
            example: {
              Email: 'string',
            },
          },
        },
      },
    })
    body: any,
  ): Promise<any> {
    try {
      let user = await this.userRepository.findOne({
        where: {Email: body.Email},
      });
      if (!user) throw new HttpErrors.BadRequest('User data not found');
      let tokenData = await this.userTokensRepository.findOne({
        where: {UserID: user.UserID},
      });
      if (tokenData) await this.userTokensRepository.deleteById(tokenData.id);
      let token = await this.userTokensRepository.create(
        new UserTokens({
          UserID: user?.UserID,
          token: crypto
            .randomBytes(48)
            .toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/\=/g, ''),
        }),
      );
      await this._mailService.sendPasswordRestMail(
        {
          to: user.Email,
          subject: 'Airshow - Reset Password',
        },
        user,
        token.token,
        token.id,
      );
      return {error: false, message: 'Reset Password link sent.'};
    } catch (err) {
      throw err;
    }
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User,{includeRelations: true}),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({resource: 'users', scopes: ['find']})
  async find(
    @param.query.object('filter', getFilterSchemaFor(User))
    filter?: Filter<User>,
  ): Promise<User[]> {
    const role = this.currentUserProfile.role;
    if (role !== 1) {
      return this.userRepository.find({
        where: {
          or: [
            {
              ClientID: this.currentUserProfile.clientID,
            },
          ],
        },
      });
    }
    return this.userRepository.find();
  }

  @get('/users/clients')
  @authenticate('jwt')
  @authorize({resource: 'users', scopes: ['find']})
  async getClients(): Promise<User[]> {
    return this.userRepository.find({
      where: {
        Type: 'client',
      },
      order: ['CreatedBy DESC', 'FirstName ASC'],
    });
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'User PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.updateAll(user, where);
  }

  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(User))
    filter?: Filter<User>,
  ): Promise<User> {
    return this.userRepository.findById(id, filter);
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
  ): Promise<void> {
    // if(!user.ClientID || user.ClientID==null){
    //   user.ClientID = ""
    // }
    await this.userRepository.updateById(id, user);
  }

  @patch('/users/profile', {
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  async profileUpdate(
    @requestBody({
      content: {
        'multipart/form-data': {
          'x-parser': 'stream',
          schema: {
            type: 'object',
          },
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<any> {
    let id = this.currentUserProfile.id;
    console.log('id:', id);
    console.log('this.currentUserProfile:', this.currentUserProfile);
    const upload = multer({dest: './public/uploads/'});
    return new Promise<object>((resolve, reject) => {
      upload.any()(request, response, async (err: any) => {
        if (err) {
          reject(err);
        } else {
          try {
            let {FirstName, LastName, HeaderColor, SidebarColor, FontColor } = request.body;
            let ProfilePic: any, Logo: any, file: any = request.files, 
            signedImageURL: any, signedLogoURL:any;

            let user = await this.userRepository.findById(id);
            if (!user) {
              throw new HttpErrors.BadRequest(`User doen't exist`);
            }
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.HeaderColor = HeaderColor;
            user.SidebarColor = SidebarColor;
            user.FontColor = FontColor;

            // if (file && file.length > 0) {
            //   const element = file[0];
            //   ProfilePic = await this.s3ImageUploadService.uploadFileToS3(
            //     element,
            //     {
            //       path: `UserProfilePics/${this.currentUserProfile.email}`,
            //       type: element.mimetype,
            //       name: `Profile`,
            //     },
            //   );
            //   user.ProfilePic = ProfilePic.Key;
            //   signedImageURL = this.s3ImageUploadService.getSignedURL(
            //     ProfilePic.Key,
            //   );
            // }
            if (file && file.length > 0) {
              for (let index = 0; index < file.length; index++) {
                const element = file[index];
                switch (element.fieldname) {
                  case 'ProfilePic':
                    ProfilePic = await this.s3ImageUploadService.uploadFileToS3(
                      element,
                      {
                        path: `UserProfilePics/${this.currentUserProfile.email}`,
                        type: element.mimetype,
                        name: `Profile`,
                      },
                    );
                    user.ProfilePic = ProfilePic.Key;
                    signedImageURL = this.s3ImageUploadService.getSignedURL(
                      ProfilePic.Key,
                    );
                    break;

                  case 'Logo':
                    Logo = await this.s3ImageUploadService.uploadFileToS3(
                      element,
                      {
                        path: `UserProfilePics/${this.currentUserProfile.email}`,
                        type: element.mimetype,
                        name: `Logo`,
                      },
                    );
                    user.Logo = Logo.Key;
                    signedLogoURL = this.s3ImageUploadService.getSignedURL(
                      Logo.Key,
                    );
                    break;
                  default:
                    break;
                }
              }
            }

            let userProfile = await this.userRepository.save(user);
            userProfile.ProfilePic = signedImageURL;
            resolve(userProfile);
          } catch (error) {
            console.log('error:', error);
            reject(error);
          }
        }
      });
    });
  }

  @put('/users/{id}', {
    responses: {
      '204': {
        description: 'User PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() user: User,
  ): Promise<void> {
    await this.userRepository.replaceById(id, user);
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.userRepository.deleteById(id);
  }
}
