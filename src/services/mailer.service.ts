import {SentMessageInfo} from 'nodemailer';
import Mail = require('nodemailer/lib/mailer');
import config from '../datasources/tricks-email-ds.datasource.config.json';
import * as handlebars from 'handlebars';
import * as fs from 'fs';
import * as path from 'path';
import {User} from '../models/index.js';
import {RestBindings, Request} from '@loopback/rest';
import {inject} from '@loopback/core';

const nodemailer = require('nodemailer');

export class MailerService {
  constructor(@inject(RestBindings.Http.REQUEST) private request: Request) {}

  async sendMail(
    mailOptions: Mail.Options,
    userData: User,
    token: string,
    id: string,
  ): Promise<SentMessageInfo> {
    const filePath = path.join(
      __dirname,
      './../../src/templates/new-user.html',
    );
    const source = fs.readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: `${userData.FirstName} ${userData.LastName}`,
      email: userData.Email,
      url: `http://airshow.fun/api/verify_new/${token}/${id}`,
      /* url: `${process.env.WEB_URL}/api/verify_new/${token}/${id}`, */
    };

    const htmlToSend = template(replacements);

    const transporter = await nodemailer.createTransport(config.transports[0]);
    mailOptions.from = '"Airshow.fun Admin" <noreply@airshow.fun>';
    mailOptions.html = htmlToSend;
    return transporter.sendMail(mailOptions);
  }

  async sendPasswordRestMail(
    mailOptions: Mail.Options,
    userData: User,
    token: string,
    id: string,
  ): Promise<SentMessageInfo> {
    const filePath = path.join(
      __dirname,
      './../../src/templates/reset-password.html',
    );
    const source = fs.readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: `${userData.FirstName} ${userData.LastName}`,
      email: userData.Email,
      url: `http://airshow.fun/api/verify/${token}/${id}`,
      /* url: `${process.env.WEB_URL}/api/verify/${token}/${id}`, */
    };
    console.log(replacements);
    const htmlToSend = template(replacements);

    const transporter = await nodemailer.createTransport(config.transports[0]);
    mailOptions.from = '"Airshow.fun Admin" <noreply@airshow.fun>';
    mailOptions.html = htmlToSend;
    return transporter.sendMail(mailOptions);
  }
}
