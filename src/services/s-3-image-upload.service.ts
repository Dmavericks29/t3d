import {bind, /* inject, */ BindingScope} from '@loopback/core';
import { join, extname } from 'path';
import { readFileSync } from 'fs';
import AWS from 'aws-sdk';
const s3 = new AWS.S3({ signatureVersion: 'v4', accessKeyId: 'AKIA5PZOFSI6JCVLDQEU' , secretAccessKey: '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' });

@bind({scope: BindingScope.TRANSIENT})
export class S3ImageUploadService {
  constructor() {}

  uploadFileToS3(file:any, options:any) {
    const buffer = file[0]?readFileSync(file[0].path) : readFileSync(file.path);
    const fileName = options.name || String(Date.now());
    const extension = file[0]?extname(file[0].originalname):extname(file.originalname);
    
    return new Promise((resolve, reject) => {
      return s3.upload({
        Bucket: 'trick3d-dev',
        Key: join(options.path, `${fileName}${extension}`),
        ContentType: options.type,
        Body: buffer,
      }, (err:any, result:any) => {
        if (err) reject(err);
        else resolve(result);
      });
    });
  };

  getSignedURL(awsFileKey:string){
      let params = {
          Bucket: 'trick3d-dev',
          Key :awsFileKey
      };
      try {
          let url = s3.getSignedUrl("getObject", params);
          return url;
      } catch (err) {
          return "";
      }
  }

  deleteFileS3(awsFileKey:string){
    let params = {
      Bucket: 'trick3d-dev',
      Delete :{
        Objects:[
          {
            Key: awsFileKey
          }
        ]
      }
    };
    return new Promise((resolve, reject) => {
      return s3.deleteObjects(params, (err:any, result:any) => {
        if (err) reject(err);
        else resolve(result);
      });
    });
  }
  
}
