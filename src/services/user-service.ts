import { HttpErrors } from '@loopback/rest';
import { Credentials, UserRepository } from '../repositories/user.repository';
import { User } from '../models/user.model';
import { UserService } from '@loopback/authentication';
import { securityId } from '@loopback/security';
import { repository } from '@loopback/repository';
import { PasswordHasher } from './hash.password.bcryptjs';
import { PasswordHasherBindings } from '../keys';
import { inject } from '@loopback/context';
import { MyUserProfile } from '../controllers/specs/user-controller.specs';

export class MyUserService implements UserService<User, Credentials> {
  constructor(
    @repository(UserRepository) public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
  ) { }

  async verifyCredentials(credentials: Credentials): Promise<User> {
    const invalidCredentialsError = 'Invalid email';
    const unRegisteredError = 'Account is not activated. Please check your email once.';

    console.log(credentials);
    const foundUser = await this.userRepository.findOne({
      where: { Email: credentials.Email, Status: true },
    });
    console.log(foundUser);
    if (!foundUser) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    const credentialsFound = await this.userRepository.findCredentials(
      foundUser.UserID,
    );
    if (!credentialsFound) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    if (!foundUser.IsActive) {
      throw new HttpErrors.UnavailableForLegalReasons(unRegisteredError);
    }

    const passwordMatched = await this.passwordHasher.comparePassword(
      credentials.Password,
      credentialsFound.Password,
    );

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(`Invalid Password. Please try again or Click on the Forgot Password link below`);
    }

    return foundUser;
  }

  convertToUserProfile(user: User): MyUserProfile {
    // since first name and lastName are optional, no error is thrown if not provided
    let userName = '';
    if (user.FirstName) userName = `${user.FirstName}`;
    if (user.LastName)
      userName = user.FirstName
        ? `${userName} ${user.LastName}`
        : `${user.LastName}`;
    return { [securityId]: user.UserID, name: userName, id: user.UserID, clientID: user.ClientID,role: user.RoleID, email: user.Email };
  }
}
