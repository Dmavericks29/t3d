import { inject } from '@loopback/context';
import { HttpErrors } from '@loopback/rest';
import { promisify } from 'util';
import { TokenService } from '@loopback/authentication';
import { securityId } from '@loopback/security';
import { TokenServiceBindings } from '../keys';
import { MyUserProfile } from '../controllers/specs/user-controller.specs';

// const jwt = require('jsonwebtoken');
var redis = require('redis');
var JWTR =  require('jwt-redis').default;
var redisClient = redis.createClient();
var jwtr = new JWTR(redisClient);
// const signAsync = promisify(jwtr.sign);
// const verifyAsync = promisify(jwtr.verify);
// const destroyAsync = promisify(jwtr.destroy);

export class JWTService implements TokenService {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SECRET)
    private jwtSecret: string,
    @inject(TokenServiceBindings.TOKEN_EXPIRES_IN)
    private jwtExpiresIn: string,
  ) { }

  async verifyToken(token: string): Promise<MyUserProfile> {
    if (!token) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : 'token' is null`,
      );
    }

    let userProfile: MyUserProfile;

    try {
      // decode user profile from token
      const decodedToken = await jwtr.verify(token, this.jwtSecret);
      // don't copy over  token field 'iat' and 'exp', nor 'email' to user profile
      userProfile = Object.assign(
        { [securityId]: '', name: '' },
        {
          [securityId]: decodedToken.id,
          name: decodedToken.name,
          id: decodedToken.id,
          email: decodedToken.email,
          clientID: decodedToken.clientID,
          role: decodedToken.role
        },
      );
    } catch (error) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : ${error.message}`,
      );
    }
    return userProfile;
  }

  async generateToken(userProfile: MyUserProfile): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized(
        'Error generating token : userProfile is null',
      );
    }
    const userInfoForToken = {
      id: userProfile[securityId],
      name: userProfile.name,
      email: userProfile.email,
      clientID: userProfile.clientID,
      role: userProfile.role,
      jti: userProfile.id
    };
    // Generate a JSON Web Token
    let token: string;
    try {
      // console.log(JSON.stringify(signAsync))
      token = await jwtr.sign(userInfoForToken, this.jwtSecret, {
        expiresIn: Number(this.jwtExpiresIn),
      });
    } catch (error) {
      throw new HttpErrors.Unauthorized(`Error encoding token : ${error}`);
    }

    return token;
  }

  async destroyToken(id: string): Promise<boolean> {
    if (!id) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : 'token' is null`,
      );
    }

    let destroyed: boolean;

    try {
      const destroy = await jwtr.destroy(id, this.jwtSecret);
      destroyed = true;
    } catch (error) {
      throw new HttpErrors.Unauthorized(
        `Error verifying token : ${error.message}`,
      );
    }
    return destroyed;
  }

}
