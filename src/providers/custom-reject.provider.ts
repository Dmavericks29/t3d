// Copyright Lokyata Inc. 2018-2019

import { HandlerContext, Reject } from '@loopback/rest';
import { Provider, inject } from '@loopback/context';
import { RestBindings, Request } from '@loopback/rest';

export class CustomRejectProvider implements Provider<Reject> {
  constructor(
    @inject(RestBindings.Http.REQUEST) public request: Request,
  ) { }

  value() {
    return (response: HandlerContext, result: Error) => {
      this.action(response, result);
    };
  }
  /**
   * Use the mimeType given in the request's Accept header to convert
   * the response object!
   * @param response - The response object used to reply to the  client.
   * @param result - The result of the operation carried out by the controller's
   * handling function.
   */
  async action({ request, response }: HandlerContext, error: Error) {
    let details: any = Object.assign({}, error);
    let errorResponse: any;

    console.log('details::', details);
    console.log('Running Port : ', request.socket.localPort);
    if (error) {
      if(details.code=='ER_DUP_ENTRY'){
        
        errorResponse = {
          message: details.sqlMessage
        }
      }else{
        errorResponse = details
      }
      response.status(400);
      response.json(errorResponse);
      response.end();

    } else {
      response.status(500);
      response.json(details);
      response.end(response);
    }
  }
}
