import { Entity, model, property } from '@loopback/repository';

@model()
export class Role extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  RoleID: number;

  @property({
    type: 'string',
    required: true,
  })
  Name: string;

  @property({
    type: 'boolean',
    default:true
  })
  Status: boolean;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  UpdatedAt?: string;

  constructor(data?: Partial<Role>) {
    super(data);
  }
}

export interface RoleRelations {
  // describe navigational properties here
}

export type RoleWithRelations = Role & RoleRelations;
