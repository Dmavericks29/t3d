import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Client, ClientWithRelations} from './client.model';

@model({
  settings: {
    foreignKeys: {
      fk_category_ClientID: {
        name: 'fk_category_ClientID',
        entity: 'Client',
        entityKey: 'ClientID',
        foreignKey: 'ClientID',
      },
    },
  },
})
export class Category extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: 'uuidv4',
  })
  CategoryID: string;

  @property({
    type: 'string',
    required: true,
    // index: {
    //   unique: true,
    // },
  })
  Name: string;

  @property({
    type: 'string',
  })
  Parent?: string;

  // @property({
  //   type: 'string',
  // })
  // ClientID: string;
  @belongsTo(() => Client, {keyFrom: 'ClientID', name: 'client'})
  ClientID: string;

  @property({
    type: 'boolean',
    default: true,
  })
  Status?: boolean;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  UpdatedAt?: string;

  @property({
    type: 'string',
    // required: true
  })
  CreatedBy?: string;

  @property({
    type: 'string',
    // required: true
  })
  UpdatedBy?: string;

  constructor(data?: Partial<Category>) {
    super(data);
  }
}

export interface CategoryRelations {
  // describe navigational properties here
  client?: ClientWithRelations;
}

export type CategoryWithRelations = Category & CategoryRelations;
