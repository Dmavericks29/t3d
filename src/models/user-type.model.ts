import { Entity, model, property } from '@loopback/repository';

@model()
export class UserType extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  TypeID?: number;

  @property({
    type: 'string',
    required: true,
  })
  Name: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  UpdatedAt?: string;

  @property({
    type: 'boolean',
    default: true,
  })
  Status: boolean;


  constructor(data?: Partial<UserType>) {
    super(data);
  }
}

export interface UserTypeRelations {
  // describe navigational properties here
}

export type UserTypeWithRelations = UserType & UserTypeRelations;
