import {
  Entity,
  model,
  property,
  belongsTo,
  hasMany,
} from '@loopback/repository';
import {Asset} from './asset.model';
import {AssetUploads} from './asset-uploads.model';

@model({
  settings: {
    foreignKeys: {
      fk_assetPicture_AssetID: {
        name: 'fk_assetPicture_AssetID',
        entity: 'Asset',
        entityKey: 'AssetID',
        foreignKey: 'AssetID',
      },
    },
  },
})
export class AssetPicture extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: 'uuidv4',
  })
  PictureID: string;

  @belongsTo(() => Asset, {keyFrom: 'AssetID', name: 'asset'})
  AssetID: string;

  @property({
    type: 'string',
    required: true,
  })
  URL: string;

  @property({
    type: 'string',
  })
  ProductVideo: string;

  @property({
    type: 'string',
    // required: true,
  })
  ProductVideoURL: string;

  @property({
    type: 'string',
    required: true,
  })
  AssetBundle: string;

  @property({
    type: 'string',
  })
  ProductImage: string;

  @hasMany(() => AssetUploads, {keyTo: 'PictureID'})
  assetUploads: AssetUploads;

  @property({
    type: 'boolean',
    default: true,
  })
  Status: boolean;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  UpdatedAt?: string;

  constructor(data?: Partial<AssetPicture>) {
    super(data);
  }
}

export interface AssetPictureRelations {
  // describe navigational properties here
}

export type AssetPictureWithRelations = AssetPicture & AssetPictureRelations;
