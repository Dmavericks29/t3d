import {Entity, model, property, hasOne, belongsTo, hasMany} from '@loopback/repository';
import { AssetMarking } from './asset-marking.model';
import { Client } from './client.model';
import { Category } from './category.model';
import { AssetPicture } from './asset-picture.model';
import {AssetBundle} from './asset-bundle.model'

@model({
  settings: {
    foreignKeys: {
      ClientID: {
        name: 'ClientID',
        entity: 'Client',
        entityKey: 'ClientID',
        foreignKey: 'ClientID',
      },
    },
  },
})
export class Asset extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn:"uuidv4"
  })
  AssetID: string;

  @belongsTo(() => Client, { keyFrom: 'ClientID', name: 'client' })
  ClientID: string;

  @belongsTo(() => Category, { keyFrom: 'CategoryID', name: 'category' })
  CategoryID: string;

  @property({
    type: 'string',
    required: true,
    unique:true
  })
  Name: string;

  @property({
    type: 'number',
    // required: true,
  })
  NoOfFeatures: number;

  @property({
    type: 'number',
    // required: true,
  })
  UploadCount: number;

  @hasMany(() => AssetMarking, { keyTo: 'AssetID' })
  assetMarking: AssetMarking;

  @hasOne(() => AssetPicture, { keyTo: 'AssetID' })
  assetPicture: AssetPicture;

  @hasOne(() => AssetBundle, { keyTo: 'AssetID' })
  assetBundle: AssetBundle;

  @property({
    type: 'string',
    jsonSchema: {
      enum: ['under-review', 'reviewed', 'published', 'unpublished'],
    },
    dafault:"under-review"
  })
  Status: string;

  @property({
    type: 'boolean',
    default:true
  })
  IsActive: boolean;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  UpdatedAt?: string;

  constructor(data?: Partial<Asset>) {
    super(data);
  }
}

export interface AssetRelations {
  // describe navigational properties here
}

export type AssetWithRelations = Asset & AssetRelations;
