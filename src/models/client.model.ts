import { Entity, model, property, hasMany } from '@loopback/repository';
import { User } from './user.model';
import { Asset } from './asset.model';

@model()
export class Client extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: "uuidv4"
  })
  ClientID: string;

  @property({
    type: 'string',
    // required: true,
  })
  FirstName: string;

  @property({
    type: 'string',
    // required: true,
  })
  LastName: string;

  @property({
    type: 'string',
    required: true,
  })
  FullName: string;

  @property({
    type: 'string',
    // required: true,
  })
  Email: string;

  @property({
    type: 'string',
    required: true,
  })
  Website: string;

  
  @property({
    type: 'string',
    required: false,
  })
  Company: string;

  @property({
    type: 'string',
    required: true,
  })
  Address: string;

  @property({
    type: 'string',
    // required: true,
  })
  Logo?: string;

  @property({
    type: 'boolean',
    default: true,
  })
  Status?: boolean;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  UpdatedAt?: string;

  @hasMany(() => User, { keyTo: 'ClientID' })
  users: User[];

  @hasMany(() => Asset, { keyTo: 'ClientID' })
  assets: Asset[];

  constructor(data?: Partial<Client>) {
    super(data);
  }
}

export interface ClientRelations {
  // describe navigational properties here
}

export type ClientWithRelations = Client & ClientRelations;
