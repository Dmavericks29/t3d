import { Entity, model, property, hasOne, hasMany, belongsTo } from '@loopback/repository';
import { UserLogin, UserLoginWithRelations } from './user-login.model';
import { UserLogs } from './user-logs.model';
import { UserRole } from './user-role.model';
import { Role, RoleWithRelations } from './role.model';
import { UserTokens } from './user-tokens.model';
import { Client } from './client.model';
import { UserSessions } from './user-sessions.model';

@model({
  settings: {
    indexes: {
      uniqueEmail: {
        keys: {
          Email: 1,
        },
        options: {
          unique: true,
        },
      },
    },
    // foreignKeys: {
    //   fk_user_ClientID: {
    //     name: 'fk_user_ClientID',
    //     entity: 'Client',
    //     entityKey: 'ClientID',
    //     foreignKey: 'ClientID',
    //   },
    // },
  },
})
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    // generated: true,
    useDefaultIdType: false,
    defaultFn: "uuidv4"
  })
  UserID: string;

  @property({
    type: 'string',
    required: true,
  })
  FirstName: string;

  @property({
    type: 'string',
    // required: true,
  })
  LastName?: string;

  @property({
    type: 'string',
    required: true,
  })
  Email: string;

  @property({
    type: 'string',
    required: true,
  })
  UserName: string;

  @property({
    type: 'string',
  })
  ProfilePic?: string;

  @property({
    type: 'string',
  })
  Logo?: string;

  @property({
    type: 'string',
  })
  HeaderColor?: string;

  @property({
    type: 'string',
  })
  SidebarColor?: string;

  @property({
    type: 'string',
  })
  FontColor?: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: ['admin', 'client'],
    }
  })
  Type: string;

  @property({
    type: 'boolean',
    default: false,
  })
  IsActive?: boolean;

  @property({
    type: 'boolean',
    default: true,
  })
  Status?: boolean;

  @belongsTo(() => Role, { keyFrom: 'RoleID', name: 'role' })
  RoleID: number;

  @belongsTo(() => Client, { keyFrom: 'ClientID', name: 'users' })
  ClientID: string;


  
  @property({
    type: 'string',
  })
  ClientName?: string;

  
  @property({
    type: 'string',
  })
  ClientCompany?: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  UpdatedAt?: string;

  @property({
    type: 'string',
  })
  CreatedBy: string;

  @property({
    type: 'string',
  })
  UpdatedBy: string;

  @hasOne(() => UserSessions, { keyTo: 'UserID' })
  userSession: UserSessions;

  @hasOne(() => UserLogin, { keyTo: 'UserID' })
  userLogin: UserLogin;

  @hasOne(() => UserRole, { keyTo: 'UserID' })
  userRole: UserRole;

  @hasOne(() => UserTokens, { keyTo: 'UserID' })
  userToken: UserTokens;

  @hasMany(() => UserLogs, { keyTo: 'UserID' })
  userLogs: UserLogs[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  role?: RoleWithRelations
  userLogin?: UserLoginWithRelations
}

export type UserWithRelations = User & UserRelations;
