import { Entity, model, property, belongsTo } from '@loopback/repository';
import { User } from './user.model';

@model()
export class UserLogs extends Entity {
  @property({
    type: 'string',
    id: true,
    // generated: true,
    useDefaultIdType: false,
    defaultFn: "uuidv4"
  })
  LogID: string;

  @belongsTo(() => User)
  UserID: string;

  @property({
    type: 'string',
    required: true,
  })
  Domain: string;

  @property({
    type: 'string',
  })
  BrowsingTime: string;

  @property({
    type: 'string',
  })
  SessionTime?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;


  constructor(data?: Partial<UserLogs>) {
    super(data);
  }
}
