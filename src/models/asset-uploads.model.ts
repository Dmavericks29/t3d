import {model, property, belongsTo, Entity} from '@loopback/repository';
import { AssetPicture } from './asset-picture.model';

@model({
  settings: {
    foreignKeys: {
      PictureID: {
        name: 'PictureID',
        entity: 'AssetPicture',
        entityKey: 'PictureID',
        foreignKey: 'PictureID',
      },
    },
  },
})
export class AssetUploads extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: "uuidv4"
  })
  AssetUploadID: string;

  @property({
    type: 'string',
    // required: true,
  })
  URL: string;

  @belongsTo(() => AssetPicture, { keyFrom: 'PictureID', name: 'assetPicture' })
  PictureID: string;

  @property({
    type: 'string',
    required: true,
  })
  Type: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  UpdatedAt?: string;

  constructor(data?: Partial<AssetUploads>) {
    super(data);
  }
}

export interface AssetUploadsRelations {
  // describe navigational properties here
}

export type AssetUploadWithRelations = AssetUploads & AssetUploadsRelations;
