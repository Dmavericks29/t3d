import {Entity, model, property, belongsTo} from '@loopback/repository';
import { User } from './user.model';

@model({
  settings: {
    foreignKeys: {
      fk_userTokens_UserID: {
        name: 'fk_userTokens_UserID',
        entity: 'User',
        entityKey: 'UserID',
        foreignKey: 'UserID',
      },
    },
  },
})
export class UserTokens extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn:'uuidv4'
  })
  id: string;

  // @property({
  //   type: 'string',
  //   required: true,
  // })
  // UserID: string;
  @belongsTo(() => User, { keyFrom: 'UserID', name: 'user' })
  UserID: string;


  @property({
    type: 'string',
    required: true,
  })
  token: string;

  @property({
    type: 'date',
    defaultFn:'now'
  })
  createdAt?: string;


  constructor(data?: Partial<UserTokens>) {
    super(data);
  }
}

export interface UserTokensRelations {
  // describe navigational properties here
}

export type UserTokensWithRelations = UserTokens & UserTokensRelations;
