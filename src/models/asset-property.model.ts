import {Model, model, property} from '@loopback/repository';

@model()
export class AssetProperty extends Model {
  @property({
    type: 'string',
    required: true,
  })
  Name: string;

  @property({
    type: 'string',
    id: true,
    generated: false,
  })
  AssetPropertyID?: string;

  @property({
    type: 'string',
    required: true,
  })
  CategoryID: string;

  @property({
    type: 'string',
    required: true,
  })
  Title: string;

  @property({
    type: 'string',
    required: true,
  })
  Description: string;

  @property({
    type: 'number',
    required: true,
  })
  MarkingNumber: number;

  @property({
    type: 'string',
  })
  ProductImage?: string;

  @property({
    type: 'string',
  })
  ProductVideo?: string;

  @property({
    type: 'string',
    required: true,
  })
  AssetID: string;

  @property({
    type: 'string',
  })
  MarkingImgURL?: string;

  @property({
    type: 'string',
  })
  MarkingVideoURL?: string;


  constructor(data?: Partial<AssetProperty>) {
    super(data);
  }
}

export interface AssetPropertyRelations {
  // describe navigational properties here
}

export type AssetPropertyWithRelations = AssetProperty & AssetPropertyRelations;
