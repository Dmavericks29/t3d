import { Entity, model, property } from '@loopback/repository';

@model()
export class AssetStatus extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  StatusID?: string;

  @property({
    type: 'string',
    required: true,
  })
  Name: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  UpdatedAt?: string;


  constructor(data?: Partial<AssetStatus>) {
    super(data);
  }
}

export interface AssetStatusRelations {
  // describe navigational properties here
}

export type AssetStatusWithRelations = AssetStatus & AssetStatusRelations;
