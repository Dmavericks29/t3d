import {Entity, model, property, belongsTo} from '@loopback/repository';
import { AssetMarking } from './asset-marking.model';

@model({
  settings: {
    foreignKeys: {
      fk_assetMarkingUploads_MarkingID: {
        name: 'fk_assetMarkingUploads_MarkingID',
        entity: 'AssetMarking',
        entityKey: 'MarkingID',
        foreignKey: 'MarkingID',
      },
    },
  },
})
export class AssetMarkingUploads extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: "uuidv4",
  })
  AssetMarkingUploadID: string;

  @belongsTo(() => AssetMarking, { keyFrom: 'MarkingID', name: 'assetMarking' })
  MarkingID: string;

  @property({
    type: 'string',
    required: true,
  })
  URL: string;

  @property({
    type: 'string',
    required: true,
  })
  Type: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  UpdatedAt?: string;


  constructor(data?: Partial<AssetMarkingUploads>) {
    super(data);
  }
}


export interface AssetMarkingUploadsRelations {
  // describe navigational properties here
}

export type AssetMarkingUploadsWithRelations = AssetMarkingUploads & AssetMarkingUploadsRelations;
