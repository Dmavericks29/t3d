import {
  Entity,
  model,
  property,
  belongsTo,
  hasMany,
} from '@loopback/repository';

import {Asset} from './asset.model';


@model({
  settings: {
    foreignKeys: {
      fk_assetBundle_AssetID: {
        name: 'fk_assetBundle_AssetID',
        entity: 'Asset',
        entityKey: 'AssetID',
        foreignKey: 'AssetID',
      },
    },
  }
})
export class AssetBundle extends Entity {
 
  @property({
    type: 'string',
    id: true,
    defaultFn: 'uuidv4',
  })
  BundleID: string;

  @belongsTo(() => Asset, {keyFrom: 'AssetID', name: 'asset'})
  AssetID: string;

  @property({
    type: 'string',
    required: true,
  })
  URL: string;

  @property({
    type: 'boolean',
    default: true,
  })
  Status: boolean;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  UpdatedAt?: string;



  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<AssetBundle>) {
    super(data);
  }
}

export interface AssetBundleRelations {
  // describe navigational properties here
}

export type AssetBundleWithRelations = AssetBundle & AssetBundleRelations;
