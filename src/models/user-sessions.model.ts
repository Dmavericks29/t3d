import {Entity, model, property, belongsTo} from '@loopback/repository';
import { User } from './user.model';

@model({
  settings: {
    foreignKeys: {
      fk_userSessions_UserID: {
        name: 'fk_userSessions_UserID',
        entity: 'User',
        entityKey: 'UserID',
        foreignKey: 'UserID',
      },
    },
  },
})
export class UserSessions extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: "uuidv4",
  })
  SessionID: string;

  // @property({
  //   type: 'string',
  //   required: true,
  // })
  // UserID: string;
  @belongsTo(() => User, { keyFrom: 'UserID', name: 'user' })
  UserID: string;

  @property({
    type: 'string',
    required: true,
  })
  Token: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  CreatedAt: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  UpdatedAt: string;

  constructor(data?: Partial<UserSessions>) {
    super(data);
  }
}

export interface UserSessionsRelations {
  // describe navigational properties here
}

export type UserSessionsWithRelations = UserSessions & UserSessionsRelations;
