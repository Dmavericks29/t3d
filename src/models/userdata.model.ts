import { Model, model, property } from '@loopback/repository';

@model()
export class Userdata extends Model {

  @property({
    type: 'string',
    id: true,
    // required: true,
    defaultFn: "uuidv4"
  })
  UserID?: string;

  @property({
    type: 'string',
    required: true,
  })
  FirstName: string;

  @property({
    type: 'string',
    // required: true,
  })
  LastName?: string;

  @property({
    type: 'string',
    required: true,
  })
  UserName: string;

  @property({
    type: 'string',
    required: true,
  })
  Email: string;

  @property({
    type: 'string',
    // required: true,
  })
  Password: string;

  @property({
    type: 'number',
    required: true,
  })
  RoleID: number;

  @property({
    type: 'string',
  })
  Type: string;

  @property({
    type: 'boolean',
    default: false,
  })
  IsActive?: boolean;

  @property({
    type: 'boolean',
    default: true,
  })
  Status?: boolean;

  @property({
    type: 'string',
  })
  ClientID?: string;

  @property({
    type: 'string',
    // required: true
  })
  CreatedBy?: string;

  @property({
    type: 'string',
    // required: true
  })
  UpdatedBy?: string;


  constructor(data?: Partial<Userdata>) {
    super(data);
  }
}

export interface UserdataRelations {
  // describe navigational properties here
}

export type UserdataWithRelations = Userdata & UserdataRelations;
