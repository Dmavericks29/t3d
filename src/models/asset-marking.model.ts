import {
  Entity,
  model,
  property,
  hasMany,
  belongsTo,
} from '@loopback/repository';
import {AssetMarkingUploads} from './asset-marking-uploads.model';
import {Asset} from './asset.model';

@model({
  settings: {
    foreignKeys: {
      fk_assetMarking_AssetID: {
        name: 'fk_assetMarking_AssetID',
        entity: 'Asset',
        entityKey: 'AssetID',
        foreignKey: 'AssetID',
      },
    },
  },
})
export class AssetMarking extends Entity {
  @property({
    type: 'string',
    id: true,
    defaultFn: 'uuidv4',
  })
  MarkingID: string;

  @belongsTo(() => Asset, {keyFrom: 'AssetID', name: 'asset'})
  AssetID: string;

  @property({
    type: 'string',
    required: true,
  })
  Title: string;

  @property({
    type: 'string',
    required: true,
  })
  Description: string;

  @property({
    type: 'string',
    // required: true,
  })
  MarkingType: string;

  @property({
    type: 'string',
  })
  MarkingImgURL?: string;

  @property({
    type: 'string',
  })
  MarkingVideoURL?: string;

  @property({
    type: 'string',
  })
  MarkingText?: string;

  @property({
    type: 'number',
    required: true,
  })
  MarkingNumber: number;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  
  UpdatedAt?: string;


  @hasMany(() => AssetMarkingUploads, {keyTo: 'MarkingID'})
  assetMarkingUploads: AssetMarkingUploads;

  constructor(data?: Partial<AssetMarking>) {
    super(data);
  }
}

export interface AssetMarkingRelations {
  // describe navigational properties here
}

export type AssetMarkingWithRelations = AssetMarking & AssetMarkingRelations;
