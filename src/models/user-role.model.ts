import { Entity, model, property } from '@loopback/repository';

@model()
export class UserRole extends Entity {
  @property({
    type: 'string',
    id: true,
    // generated: true,
    useDefaultIdType: false,
    defaultFn: "uuidv4"
  })
  UserRoleID: string;

  @property({
    type: 'number',
    required: true,
  })
  RoleID: number;

  @property({
    type: 'string',
    required: true,
  })
  UserID: string;

  @property({
    type: 'string',
  })
  userUserId?: string;

  constructor(data?: Partial<UserRole>) {
    super(data);
  }
}

export interface UserRoleRelations {
  // describe navigational properties here
}

export type UserRoleWithRelations = UserRole & UserRoleRelations;
