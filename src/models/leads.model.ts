import {Entity, model, property} from '@loopback/repository';
import { int } from 'aws-sdk/clients/datapipeline';

@model()
export class Leads extends Entity {

  @property({
    type: 'string',
    id: true,
    defaultFn: 'uuidv4',
  })
  LeadID: number;

  @property({
    type: 'string',
  })
  FirstName?: string;

  @property({
    type: 'string',
  })
  LastName?: string;

  @property({
    type: 'string',
  })
  Website?: string;

  @property({
    type: 'string',
    
  })
  Email: string;

  @property({
    type: 'number',
  })
  Phone?: number;

  @property({
    type: 'number',
  })
  Mobile?: number;

  @property({
    type: 'string',
    
  })
  CompanyName: string;

  @property({
    type: 'string',
  })
  Address?: string;

  @property({
    type: 'date',
    defaultFn: 'now',
  })
  CreatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Leads>) {
    super(data);
  }
}

export interface LeadsRelations {
  // describe navigational properties here
}

export type LeadsWithRelations = Leads & LeadsRelations;
