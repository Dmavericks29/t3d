import { Entity, model, property, belongsTo } from '@loopback/repository';
import { UserWithRelations, User } from './user.model';
// import { User, UserWithRelations } from './user.model';

@model({
  settings: {
    foreignKeys: {
      UserID: {
        name: 'UserID',
        entity: 'User',
        entityKey: 'UserID',
        foreignKey: 'UserID',
      },
    },
  },
})
export class UserLogin extends Entity {
  @property({
    type: 'string',
    id: true,
    // generated: true,
    useDefaultIdType: false,
    defaultFn: "uuidv4"
  })
  LoginID: string;

  @property({
    type: 'string',
    required: true,
  })
  Password: string;

  @property({
    type: 'date',
    defaultFn: "now",
  })
  LostLoginAt?: string;

  // @property({
  //   type: 'string',
  //   required: true,
  // })
  // UserID: string;
  @belongsTo(() => User, { keyFrom: 'UserID', name: 'user' })
  UserID: string;


  constructor(data?: Partial<UserLogin>) {
    super(data);
  }
}

export interface UserLoginRelations {
  user?: UserWithRelations
}

export type UserLoginWithRelations = UserLogin & UserLoginRelations;
